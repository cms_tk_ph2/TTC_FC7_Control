# TTC_FC7_Control

### Setup on CC7 (scroll down for instructions on setting up on SLC6)

1. Check which version of gcc is installed on your CC7, it should be > 4.8 (could be the default on CC7):

        $> gcc --version

2. On CC7 you also need to install boost v1.53 headers (default on this system) and pugixml as they don't ship with uHAL any more:

        $> sudo yum install boost-devel
        $> sudo yum install pugixml-devel

2. Install uHAL. SW tested with uHAL version up to 2.7.1

        Follow instructions from
        https://ipbus.web.cern.ch/ipbus/doc/user/html/software/install/yum.html

3. Install CERN ROOT

        $> sudo yum install root
        $> sudo yum install root-net-http root-net-httpsniff  root-graf3d-gl root-physics root-montecarlo-eg root-graf3d-eve root-geom libusb-devel xorg-x11-xauth.x86_64


5. Install CMAKE > 2.8:

        $> sudo yum install cmake
        $> sudo yum -y install xterm (to eudaq)

### Install TTC_FC7_Control

        &> git clone https://gitlab.cern.ch/cms_tk_ph2/TTC_FC7_Control.git
        &> cd TTC_FC7_Control
        &> source setup.sh
        &> mkdir build
        &> cd build
        &> cmake ..
        &> make -j<number_of_cores>

### Doing a calibration test

        &> mkdir test_calibration
        &> cd test_calibration
        &> calibration_example -f ../settings/PS_HalfModule.xml

The options for running are found in `settings/PS_HalfModule.xml`.

### Doing a eudaq test

Clone and compile the eudaq framework, just run the following commands in the parent directory from TTC_FC7_Control :

	&> git clone --single-branch --branch=cms_tk_master https://gitlab.cern.ch/cms_tk_ph2/eudaq.git eudaq
	&> cd eudaq
	&> mkdir build
	&> cd build
	&> cmake ..
	&> make install

To run  
  &> source eudaq_setup.sh
  &> mkdir test_eudaq
  &> cd test_eudaq
  &> ./eudaq_test.sh
  &> eudaqproducer -r tcp://localhost:44000 -n Eudaq2Producer

Your ssh session should allow for graphical interfaces.

### Available script (-h to get help)
        $> fpgaconfig
