/*!

 *
 * \file CicFEAlignment.h
 * \brief CIC FE alignment class, automated alignment procedure for CICs
 * connected to FEs
 * \author Sarah SEIF EL NASR-STOREY
 * \author2 Younes OTARID
 * \date 13 / 11 / 19
 *
 * \Support : sarah.storey@cern.ch
 * \Support2 : younes.otarid@desy.de
 *
*/

//#include "CBCChannelGroupHandler.h"
//#include "Channel.h"
//#include "CicFEAlignment.h"
//#include "ContainerFactory.h"
//#include "Occupancy.h"

#include "../System/TTC_FC7Controller.h"
#include "../Utils/Timer.h"
#include <cstdlib>
#include <string>
#include <iostream>

using namespace TTC_FC7_HwDescription;
using namespace TTC_FC7_HwInterface;
using namespace TTC_FC7_System;
using namespace TTC_FC7_Utils; // Decoder, SettingsMap, tr_logger_sel_data

//#include "../../Ph2_ACF_forkXanda/System/SystemController.h"
//using namespace Ph2_System;

#ifdef __EUDAQ__
#include "Eudaq2Producer_TTC_FC7.h"

// std::map<Chip*, uint16_t> CicFEAlignment::fVplusMap;

Eudaq2Producer_TTC_FC7::Eudaq2Producer_TTC_FC7(const std::string& name, const std::string& runcontrol)
    : Tool(), eudaq::Producer(name, runcontrol), fExitRun(false)
{
    fPh2FileHandler_TTC_FC7   = nullptr;
    fPh2FileHandler_TTC_FC7_decoded   = nullptr;
    //fSLinkFileHandler_TTC_FC7 = nullptr;
    fInitialised = false; 
    fConfigured  = false; 
}

Eudaq2Producer_TTC_FC7::~Eudaq2Producer_TTC_FC7()
{
  delete fPh2FileHandler_TTC_FC7;
  delete fPh2FileHandler_TTC_FC7_decoded;
  //delete fSLinkFileHandler_TTC_FC7;
}

void Eudaq2Producer_TTC_FC7::Initialise() { }

void Eudaq2Producer_TTC_FC7::DoInitialise()
{
    bool no_run_control = false;
    LOG(INFO) << "Initialising producer..." << RESET;
    auto cEudaqIni = GetInitConfiguration();
    fInitialised = true;
    // LOG (INFO) << "  INITIALIZE ID: " << ini->Get("initid", 0) << std::endl;
    // EUDAQ_INFO("TLU INITIALIZE ID: " + std::to_string(ini->Get("initid", 0)));

    // Get TTC_FC7 configuration path
    std::string fPathToHWFile_name = "./settings/PS_HalfModule.xml";
    if (no_run_control)
    {
        std::string base_dir = std::getenv("TTC_FC7_BASE_DIR");
        fPathToHWFile_OT_FC7 = base_dir + "/settings/TTC_FC7Description_skeleton.xml"; 
    }
    else 
    {
        fPathToHWFile_OT_FC7 = cEudaqIni->Get("HWFile", "./settings/PS_HalfModule.xml");
    }
    LOG(INFO) << BOLDYELLOW << "Loading settings to TTC-FC7 from file : " << fPathToHWFile_OT_FC7 << RESET;

    std::stringstream outp;
    // Outer Tracker hardware configuration
    this->InitializeHw(fPathToHWFile_OT_FC7);
    this->InitializeSettings(fPathToHWFile_OT_FC7, outp);
    LOG(INFO) << outp.str();

    this->ConfigureHw();
    this->print_version();

    this->configure();
    this->system_checks();

}

void Eudaq2Producer_TTC_FC7::DoConfigure()
{
    auto cEudaqConf = GetConfiguration();

    //this->configure();
    //this->system_checks();

    /*LOG(INFO) << "Configuring producer..." << RESET;
    fHWFile = conf->Get("HWFile", "./settings/PS_HalfModule.xml");
    std::stringstream outp;
    this->InitializeHw(cHWFile, outp);
    this->InitializeSettings(cHWFile, outp);
    this->ConfigureHw();
    this->print_version();
    */
    // only thing I don't understand is where the run number goes
    // getting the configuration
    /*
    auto              cRunNumber = GetRunNumber();
    auto              conf       = GetConfiguration();
    std::stringstream outp;
    fHWFile = conf->Get("HWFile", "./settings/D19CDescription.xml");

    // initialisng ph2acf
    this->InitializeHw(fHWFile, outp);
    this->InitializeSettings(fHWFile, outp);
    LOG(INFO) << outp.str();
    TTC_FC7* theFirstBoard = static_cast<TTC_FC7*>(fDetectorContainer->at(0));

    // configure hardware
    this->ConfigureHw();
    fHandshakeEnabled    = (this->fTTC_FC7Interface->ReadBoardReg(theFirstBoard, "fc7_daq_cnfg.readout_block.global.data_handshake_enable") > 0);
    fTriggerMultiplicity = this->fTTC_FC7Interface->ReadBoardReg(theFirstBoard, "fc7_daq_cnfg.fast_command_block.misc.trigger_multiplicity");

    // now do cic alignment if needed
    // udtc does not support different front-end types that is why checking for only board 0 is enough
    if(theFirstBoard->getFrontEndType() == FrontEndType::CIC)
    {
        // CIC FE alignment tool
        CicFEAlignment cCicAligner;
        cCicAligner.Inherit(this);
        cCicAligner.Initialise();

        // run phase alignment

        bool cPhaseAligned = cCicAligner.PhaseAlignment(50);
        if(!cPhaseAligned)
        {
            LOG(INFO) << BOLDRED << "FAILED " << BOLDBLUE << " phase alignment step on CIC input .. " << RESET;
            exit(0);
        }
        LOG(INFO) << BOLDGREEN << "SUCCESSFUL " << BOLDBLUE << " phase alignment on CIC inputs... " << RESET;

        // run word alignment
        bool cWordAligned = cCicAligner.WordAlignment(false);
        if(!cWordAligned)
        {
            LOG(INFO) << BOLDRED << "FAILED " << BOLDBLUE << "word alignment step on CIC input .. " << RESET;
            exit(0);
        }
        LOG(INFO) << BOLDGREEN << "SUCCESSFUL " << BOLDBLUE << " word alignment on CIC inputs... " << RESET;

        // manually set bx0 alignment and package delay in firmware [needed for stub packing]
        uint8_t cBx0DelayCIC = 8;
        bool    cBxAligned   = cCicAligner.SetBx0Delay(cBx0DelayCIC);
        // bool cBxAligned = cCicAligner.Bx0Alignment(4, 0 , 1, 100);
        if(!cBxAligned)
        {
            LOG(INFO) << BOLDRED << "FAILED " << BOLDBLUE << " bx0 alignment step in CIC ... " << RESET;
            exit(0);
        }
        LOG(INFO) << BOLDGREEN << "SUCCESSFUL " << BOLDBLUE << " bx0 alignment step in CIC ... " << RESET;
    }
    */

    // done configuration
    fConfigured = true;
}

void Eudaq2Producer_TTC_FC7::DoStartRun()
{
    LOG(INFO) << "Starting sending triggers..." << RESET;

    //int Trigger_rate_UserDefFreq = std::stoi(cEudaqConf->Get("send_triggers", "20"));
    int Trigger_rate_UserDefFreq  = this->findValueInSettings("Trigger_rate_UserDefFreq");
    LOG(INFO) << BOLDYELLOW << "Changing settings from file : Trigger_rate_UserDefFreq = " << Trigger_rate_UserDefFreq << RESET;
    this->send_trigger(Trigger_rate_UserDefFreq);
    LOG(INFO) << "Starting Run..." << RESET;

    // read the version
    LOG(INFO) << "Run Started, number of triggers received so far: " << +this->fTTC_FC7Interface->ReadBoardReg("user.stat_regs.counter_1.evcnt");

    ///*
    auto conf       = GetConfiguration();
    auto cRunNumber = GetRunNumber();
    // file handlers for Ph2ACF raw + s-link
    //fRawPh2ACF = conf->Get("RawDataDirectory", "/tmp/") + "Run_" + std::to_string(cRunNumber) + ".raw";
    fRawPh2ACF = "Run_" + std::to_string(cRunNumber) + ".raw"; //conf->Get("RawDataDirectory", "/tmp/") + "Run_" + std::to_string(cRunNumber) + ".raw";
    LOG(INFO) << BOLDBLUE << "Writing raw TTC_FC7 data to " << fRawPh2ACF << " cRunNumber = " << cRunNumber << RESET;

    fPh2FileHandler_TTC_FC7          = new FileHandler_TTC_FC7(fRawPh2ACF, 'w');

    this->fTTC_FC7Interface->Start();
    LOG(INFO) << BOLDBLUE << "Shutter opened on board " << RESET;
    // set trigger source

    // starting readout loop in thread
    fExitRun   = false;
    count_evt_sent = 0;
    fThreadRun = std::thread(&Eudaq2Producer_TTC_FC7::ReadoutLoop, this);


}

void Eudaq2Producer_TTC_FC7::DoStopRun()
{
    bool offline_decoding = true;
    LOG(INFO) << "Stopping triggers..." << RESET;
    this->disable_triggers();
    this->read_trigger_enable_state();
    LOG(INFO) << "Stopping Run..." << RESET;
    fExitRun = true;

    LOG(INFO) << BOLDBLUE << "Sent to eudaq " << count_evt_sent << " events" << RESET;

    LOG(INFO) << BOLDBLUE << "Closing shutter..." << RESET;
    this->fTTC_FC7Interface->Stop();
    LOG(INFO) << BOLDBLUE << "Shutter closed on TTC-FC7 board " << RESET;
    if(fThreadRun.joinable()) { fThreadRun.join(); }
    LOG(INFO) << "Run Stopped, number of triggers received so far: " << +this->fTTC_FC7Interface->ReadBoardReg("user.stat_regs.counter_1.evcnt");

    // check if file handler is open
    if(fPh2FileHandler_TTC_FC7->isFileOpen())
    {
        LOG(INFO) << BOLDBLUE << "Closing file handler for " << fRawPh2ACF << RESET;
        fPh2FileHandler_TTC_FC7->closeFile();
    }


    fPh2FileHandler_TTC_FC7_read          = new FileHandler_TTC_FC7(fRawPh2ACF, 'r');
    //if(fFileHandler_TTC_FC7->isFileOpen() == true)
    //{
      std::vector<uint32_t> result;
      result = fPh2FileHandler_TTC_FC7_read->readFile_bin();
      //fPh2FileHandler_TTC_FC7_read->closeFile();
      LOG(INFO) << BOLDBLUE << "Read the result "<< RESET;
    //}

    if (result.size() > 0)
    {
      Timer time_to_decode;
      time_to_decode.start();
      // decode raw data
      auto conf       = GetConfiguration();
      auto cRunNumber = GetRunNumber();
      //fRawPh2ACF_decoded = conf->Get("RawDataDirectory", "/tmp/") + "Run_" + std::to_string(cRunNumber) + ".decoded";
      fRawPh2ACF_decoded = "Run_" + std::to_string(cRunNumber) + ".decoded";
      fPh2FileHandler_TTC_FC7_decoded          = new FileHandler_TTC_FC7(fRawPh2ACF_decoded, 'w');
      LOG(INFO) << BOLDBLUE << "Opening file to save decoded " << fRawPh2ACF_decoded << RESET;

      //this->addFileHandler_TTC_FC7(ttc_log_dump_file_decoded , 'w');
      //this->initializeWriteFileHandler_TTC_FC7();
      int counter_orbit = 0;
      bool verbose = 0;
      bool test_read_write = false;
      Decoder decoder = Decoder();
      for (uint32_t const&  yy : result)
      {
        if ( test_read_write ) std::cout << this->textBinaryVersion(yy) << std::endl;
        uint32_t yy_test = yy;
        struct tr_logger_sel_data result_log_dump = decoder.decode_log(yy_test, !offline_decoding);
        //LOG(INFO) << BOLDRED << "Decoded result. " << RESET;
        if ( result_log_dump.type_log != "Err" )
        {
          int counter_orbit_local = this->Write_ttc_log_dump(result_log_dump, verbose, fPh2FileHandler_TTC_FC7_decoded);
          //LOG(INFO) << BOLDRED << "Decoded result 2. " << RESET;
          counter_orbit += counter_orbit_local;
        } else
        {
          this->Write_raw_log(yy, true, verbose);
          LOG(INFO) << BOLDRED << "Could not decode the entry. " << counter_orbit << RESET;
        }

      }
      LOG(INFO) << YELLOW << "There was " << counter_orbit <<"(" << result.size() << ")" << " orbits (entries) in the run." << RESET;
      this->closeFileHandler_TTC_FC7();
      time_to_decode.stop();
      LOG(INFO) << YELLOW << "Time to decode: " << time_to_decode.getElapsedTime() << " us" << RESET;
      //delete fPh2FileHandler_TTC_FC7;
    }

    // check if file handler is open
    /*if(fSLinkFileHandler_TTC_FC7->isFileOpen())
    {
        LOG(INFO) << BOLDBLUE << "Closing file handler for .daq " << RESET;
        fSLinkFileHandler_TTC_FC7->closeFile();
    }*/
    //delete fPh2FileHandler_TTC_FC7;

}

void Eudaq2Producer_TTC_FC7::DoReset()
{
    // just in case close the shutter
    this->fTTC_FC7Interface->Stop();
    //cReadoutInterface->ResetReadout();
    LOG(INFO) << BOLDBLUE << "Reset readout on TTC_FC7" << RESET;
    LOG(INFO) << "Reset, number of triggers received so far: " << +this->fTTC_FC7Interface->ReadBoardReg("user.stat_regs.counter_1.evcnt");
    //this->fTTC_FC7Interface->Reset();
    // finish data processing
    fExitRun = true, fConfigured = false;
    if(fThreadRun.joinable()) { fThreadRun.join(); }

    // configure the board again
    this->DoConfigure();
    fConfigured = true;

}

void Eudaq2Producer_TTC_FC7::DoTerminate()
{
    fExitRun = true, fInitialised = false, fConfigured = false;
    //if(fThreadRun.joinable()) { fThreadRun.join(); }
    //LOG(INFO) << "Terminating ...";
    //this->Destroy();

    LOG(INFO) << "[TTC-FC7 Producer] Terminating Run..." << RESET;
    if(fThreadRun.joinable()) { fThreadRun.join(); }
    this->Destroy();
    LOG(INFO) << "[TTC-FC7 Producer] Terminated Run" << RESET;
    EUDAQ_INFO("[TTC-FC7 Producer] SUCESS : Terminated Run");
}

// ReadoutLoop has been modified in order to allow for the acquisition of multilple events by a single trigger signal.
// This way, time walk performance can be evaluated in the analysis
// Multiple Ph2ACF Events are read, converted and stored as EUDAQ SubEvents within in one EUDAQ Event
void Eudaq2Producer_TTC_FC7::ReadoutLoop()
{
    ///*
    LOG(INFO) << "ReadoutLoop ...";
    //std::vector<Event_TTC_FC7*> cPh2Events; // Event vector to store newly read data and previously remaining one
    // uint32_t
    std::vector<uint32_t> cPh2Events;

    while(!fExitRun)
    {
        //if(!EventsPending()) { continue; }

        //TTC_FC7*              theBoard = static_cast<TTC_FC7*>(cBoard);
        std::vector<uint32_t> cRawData(0);
        bool online_decoding = false;
        int verbose = 0;
        int count_n_full = 0;
        uint32_t data_size = this->ReadData(cRawData, count_n_full, online_decoding, verbose);
        if(cRawData.size() == 0)
        {
            LOG(INFO) << BOLDBLUE << "Read-back 0 words from the memory using ReadData.. waiting 100 ms " << RESET;
            std::this_thread::sleep_for(std::chrono::microseconds(100));
            continue;
        }
        // if data size is saturated throw error????
        uint32_t Threshold = 10000; // FIXME put in definitions and fix the number
        if (data_size > Threshold) 
        {
            LOG(INFO) << BOLDRED << "Warning, FIFO was saturated " << RESET;
        }

        // loop to ecord the FIFO
        //bool verbose = false;
        //bool test_read_write = false;
        //bool log_in_binary = true; // check if add this as formatting the raw data saving for decoding
        //Decoder decoder = Decoder();
        fPh2FileHandler_TTC_FC7->setData(cRawData);
        //std::vector<Event_TTC_FC7*> cPh2NewEvents = this->GetEvents();
        //std::vector<uint32_t> cPh2NewEvents = this->GetEvents();
        std::vector<struct tr_logger_sel_data> cPh2NewEvents = this->GetEvents();
        if(cPh2NewEvents.size() == 0)
        {
            LOG(INFO) << BOLDBLUE << "Read 0 valid events.. not going to send anything ... (cRawData size = " << cRawData.size() << ")" << RESET;
            continue;
        }
        //LOG(INFO) << BOLDBLUE << cPh2NewEvents.size() << " events read back from FC7 with ReadData" << RESET;
        //ph2acf stuff to check if relevant here -- std::move(cPh2NewEvents.begin(), cPh2NewEvents.end(), std::back_inserter(cPh2Events));
        std::time_t cTimestamp = std::time(nullptr);
        
        //uint32_t fTriggerMultiplicity = 0; // ph2acf stuff to check if relevant here
        //while(cPh2Events.size() > fTriggerMultiplicity)
        //{
            eudaq::EventSP cEudaqEvent = eudaq::Event::MakeShared("CMSPhase2RawEvent");
            cEudaqEvent->SetTimestamp(cTimestamp, cTimestamp);
            // Add multiple TTC_FC7 Events as EUDAQ SubEvents to a EUDAQ Event
            //for(auto cPh2Event = cPh2Events.begin(); cPh2Event < cPh2Events.begin() + fTriggerMultiplicity + 1; cPh2Event++)
            for (struct tr_logger_sel_data cPh2Event : cPh2NewEvents)
            {

                eudaq::EventSP cEudaqSubEvent = eudaq::Event::MakeShared("CMSPhase2RawEvent");
                //if (!offline_decoding) 
                struct tr_logger_sel_data cPh2Event_loc = cPh2Event;
                this->ConvertToSubEvent(cPh2Event_loc, cEudaqSubEvent, online_decoding);
                cEudaqSubEvent->SetTimestamp(cTimestamp, cTimestamp);
                cEudaqEvent->AddSubEvent(cEudaqSubEvent);
                count_evt_sent++;
            }
            //cPh2Events.erase(cPh2Events.begin(), cPh2Events.begin() + fTriggerMultiplicity + 1);
            SendEvent(cEudaqEvent);
            
        //}
    }     // end of !fExitRun loop

}

//void Eudaq2Producer_TTC_FC7::ConvertToSubEvent(const Event_TTC_FC7* pPh2Event, eudaq::EventSP pEudaqSubEvent)
void Eudaq2Producer_TTC_FC7::ConvertToSubEvent(const struct tr_logger_sel_data pPh2Event, eudaq::EventSP pEudaqSubEvent, bool )
{
    // by now this will be only the decoding
    pEudaqSubEvent->SetTag("RAW_EVENTS",   +pPh2Event.raw_data);
    pEudaqSubEvent->SetTag("TYPE_log",      pPh2Event.type_log);
    pEudaqSubEvent->SetTag("type_result1",  pPh2Event.type_result1);
    pEudaqSubEvent->SetTag("type_result2",  pPh2Event.type_result2);
    pEudaqSubEvent->SetTag("result1",      +pPh2Event.result1 );
    pEudaqSubEvent->SetTag("result2",      +pPh2Event.result2);
    pEudaqSubEvent->SetTag("log_result",    pPh2Event.log_result);
}

bool Eudaq2Producer_TTC_FC7::EventsPending()
{
    /*
    if(fConfigured)
    {
        if(fHandshakeEnabled)
        {
            for(auto cBoard: *fDetectorContainer)
            {
                TTC_FC7* theBoard = static_cast<TTC_FC7*>(cBoard);
                if(theBoard->getBoardType() == BoardType::D19C)
                {
                    if(this->fTTC_FC7Interface->ReadBoardReg(theBoard, "fc7_daq_stat.readout_block.general.readout_req") > 0) { return true; } // end of if ReadBoardReg
                }                                                                                                                              // end of if BoardType
            }                                                                                                                                  // end of theBoard loop
        }
        else
        {
            return true;
        } // end of if fHandshakeEnabled

    } // end of if fConfigured
    */
    return false;
}

// FIXME check me Sarah
void Eudaq2Producer_TTC_FC7::writeObjects()
{
    // this->SaveResults();
    // fResultFile->Flush();
}
#endif
