/*!
 *
 * \file CicFEAlignment.h
 * \brief CIC FE alignment class, automated alignment procedure for CICs
 * connected to FEs
 * \author Sarah SEIF EL NASR-STOREY
 * \author2 Younes OTARID
 * \date 13 / 11 / 19
 *
 * \Support : sarah.storey@cern.ch
 * \Support2 : younes.otarid@desy.de
 *
 */

#ifndef Eudaq2Producer_TTC_FC7_h__
#define Eudaq2Producer_TTC_FC7_h__

#include "Tool.h"

#include <cmath>
#include <map>
#include <memory>
#include <stdlib.h>

#ifdef __USE_ROOT__
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "TH2.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TString.h"
#include "TText.h"
#endif

// eudaq stuff
#ifdef __EUDAQ__
#include "eudaq/Configuration.hh"
#include "eudaq/Event.hh"
#include "eudaq/Factory.hh"
#include "eudaq/Logger.hh"
#include "eudaq/OptionParser.hh"
#include "eudaq/Producer.hh"
#include "eudaq/RawEvent.hh"
#include "eudaq/Time.hh"
#include "eudaq/Utils.hh"
#endif

#ifdef __EUDAQ__
class Eudaq2Producer_TTC_FC7
    : public Tool
    , public eudaq::Producer
{
  public:
    Eudaq2Producer_TTC_FC7(const std::string& name, const std::string& runcontrol);
    ~Eudaq2Producer_TTC_FC7();

    // ph2 acf tool init
    void Initialise();
    void writeObjects();

    // to offload overriden methods a bit
    void ReadoutLoop();
    //void ConvertToSubEvent(const TTC_FC7_HwInterface::Event_TTC_FC7*, eudaq::EventSP, bool online_decoding);
    void ConvertToSubEvent(const struct tr_logger_sel_data, eudaq::EventSP, bool online_decoding);
    bool EventsPending();

    // override initialization from euDAQ
    void DoConfigure() override;
    void DoInitialise() override;
    void DoStartRun() override;
    void DoStopRun() override;
    void DoTerminate() override;
    void DoReset() override;
    // void RunLoop() override; //is replaced by ReadOutLoop()

    // register producer in eudaq2
    static const uint32_t m_id_factory = eudaq::cstr2hash("CMSPhase2Producer");

  protected:
  private:
    // settings
    std::string  fPathToHWFile_OT_FC7;
    bool        fHandshakeEnabled;
    uint32_t    fTriggerMultiplicity;
    uint32_t    fHitsCounter;
    std::string fHWFile;
    std::string fRawPh2ACF;
    std::string fRawPh2ACF_decoded;

    // status variables
    bool        fInitialised, fConfigured, fExitRun;
    std::thread fThreadRun;
    int count_evt_sent;

    // for raw data
    FileHandler_TTC_FC7* fPh2FileHandler_TTC_FC7;
    FileHandler_TTC_FC7* fPh2FileHandler_TTC_FC7_decoded;
    FileHandler_TTC_FC7* fPh2FileHandler_TTC_FC7_read;
    // for s-link data [TBD]
    //FileHandler_TTC_FC7* fSLinkFileHandler_TTC_FC7;
};

// Register Producer in EUDAQ Factory
namespace
{
auto dummy0 = eudaq::Factory<eudaq::Producer>::Register<Eudaq2Producer_TTC_FC7, const std::string&, const std::string&>(Eudaq2Producer_TTC_FC7::m_id_factory);
}

#endif
#endif
