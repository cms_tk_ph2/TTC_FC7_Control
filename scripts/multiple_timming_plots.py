#!/usr/bin/env python
import os
import matplotlib as mpl
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
from matplotlib import pyplot as plt
import matplotlib.lines as lines
import glob
from scipy.optimize import curve_fit
import numpy as np

def power_law(x, a, b):
    return b*np.power(x, 1)
#import pandas as pd a*np.power(x, 0) +

fig = plt.figure()
plt.xlim([0.1, 1000.0])
## dec
# Without decoding: ["running_tests_4","running_tests_8", "running_tests_9", "running_tests_10", "running_noStrConv"]
# "running_tests_10",   "running_randisk_tmpfs_512m", "running_randisk_ramfs_512m"
# decoding ["running_tests_3","running_tests_5", "running_tests_6", "running_tests_7", "running_timming_xtaldaq"]
## from xtaldaq: running_timming_xtaldaq

## nov
#["running_timming_decoding", "running_timming_decoding_2", "running_timming_decoding_1"  , "running_timming_decoding_3" ]
#["running_timming_Notdecoding_1", "running_timming_Notdecoding_3", "running_timming_Notdecoding_2"]



colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
for rr, run_test in enumerate([ "running_baseline_raw_pin7",  "running_baseline_decoded_pin7"]) : # "conditions_2/running_tests_1", "from_06_12_2021/running_tests_10","conditions_2/running_time_per_read_2",
    list_files =glob.glob("../"+ run_test + "/ttc_log_dump/*raw")
    #file_handle = open("ttc_log_dump/ttc_log_dump_file_timming.raw", "r")
    trigger_interval = []
    trigger_frequency = []
    nentries = []
    nentries_average = []
    nentries_lenght = []
    nentries_variance = []
    ndrains = []
    for xx in list_files:
      #print(xx)
      file_handle = open(xx.replace(".raw", ".log"), "r")
      for yy in file_handle:
      #  print(xx, float(xx.split()[1]))
        if "no of FIFO drains" in yy :
            ndrains = ndrains + [int(yy.split(":")[1])]
        if "trigger interval" in yy :
            trigger_interval = trigger_interval + [float(yy.split(":")[1])]
            trigger_frequency = trigger_frequency + [40000./float(yy.split(":")[1])]
        if "nentries "in yy :
            list_nentries = yy.split(":")[1].split()
            list_nentries = list(filter(lambda x: x != '131074', list_nentries))
            #nentries = []
            #for ii, zz in enumerate(list_nentries) :

            nentries = [int(zz) for ii, zz in enumerate(list_nentries) if (ii > len(list_nentries)/10) ] # if (ii > 11 or len(list_nentries) < 13)
            #print (list_nentries)
            nentries_average = nentries_average + [sum(nentries)/len(nentries) if len(nentries) > 0 else 131074]
            nentries_lenght = nentries_lenght + [len(list_nentries)]

    nentries_average_to_fit = nentries_average
    trigger_frequency_to_fit = trigger_frequency
    trigger_frequency_to_plot = trigger_frequency

    nentries_average_to_fit = list(filter(lambda x: x != 131074, nentries_average_to_fit))
    shift = 15
    if "running_tests_10" in run_test  : shift = 4
    n_to_remove = len(nentries_average_to_fit)-shift
    print(nentries_average_to_fit)
    #print(run_test, len(nentries_average), len(trigger_frequency_to_plot))

    #del nentries_average_to_fit[n_to_remove:len(nentries_average_to_fit)]
    #del trigger_frequency_to_fit[n_to_remove:len(nentries_average)]
    #trigger_frequency_to_fit.pop(n_to_remove:len(nentries_average_to_fit))
    nentries_average_to_fit = nentries_average_to_fit[:n_to_remove]
    trigger_frequency_to_fit = trigger_frequency_to_fit[:n_to_remove] #+ trigger_frequency_to_fit[index+1 :]

    #print(run_test, len(nentries_average), len(nentries_average_to_fit), len(trigger_frequency_to_fit))
    #print(run_test, len(nentries_average), len(trigger_frequency_to_plot))

    #print(nentries_average_to_fit)
    ## remove saturated and last 3 points
    line_lenght = 2
    if run_test == "running_randisk_tmpfs_512m" : line_lenght = 3
    size_point = "."
    if rr==0 : size_point = "o"
    pars, cov = curve_fit(f=power_law, xdata=trigger_frequency_to_fit, ydata=nentries_average_to_fit, p0=[0, 0], bounds=(-np.inf, np.inf)) #
    #print(run_test, pars, pars[1])
    plt.plot(trigger_frequency, power_law(trigger_frequency, *pars), linestyle='--', linewidth=1, color=colors[rr])
    plt.plot(trigger_frequency_to_plot, nentries_average, marker=size_point, linestyle='dashed', linewidth = 0, markersize=12, color=colors[rr], label=run_test.replace("running_","").replace("tests_", "run standard ").replace("from_06_12_2021/", "(6/12) ").replace("conditions_2/", "(7/12) ").replace("time_per_read", "").replace("_2us_delay", " 200us delay").replace("_2ms_delay", " 2000us delay") + "; slope = " + str(round(pars[1],2)) )
    plt.plot([trigger_frequency[n_to_remove+shift], trigger_frequency[n_to_remove+shift]], [1.0, 131074.0], linestyle='solid', linewidth = line_lenght, markersize=0, color=colors[rr])
    plt.plot([trigger_frequency[n_to_remove], trigger_frequency[n_to_remove]], [1.0, 131074.0], linestyle='dotted', linewidth = line_lenght, markersize=0, color=colors[rr])

#plt.plot([10, 1000], [13107400.0, 13107400.0], linestyle='dashed', linewidth = 1, markersize=0)
plt.plot([10, 1000], [131074.0, 131074.0], linestyle='dashed', linewidth = 1, markersize=0)
#fig.add_artist(lines.Line2D([600, 60000], [131074.0, 131074.0]))
#plt.ylim( 0, 1500 )
#plt.xlim( 0, 80 )
plt.xscale('log')
plt.yscale('log')
plt.title('Without decoding')
#plt.title('Decoding (6/12/2021)')
plt.ylabel( 'average # entries per FIFO drain', fontsize=15)
plt.xlabel('trigger rate (kHz)', fontsize=15)
plt.legend(loc='upper left')
plt.savefig("ttc_log_dump_file_timming_in_frequency_multiple.png")
plt.figure().clear()
