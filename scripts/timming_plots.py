#!/usr/bin/env python
import os
import matplotlib as mpl
#if os.environ.get('DISPLAY','') == '':
#    print('no display found. Using non-interactive Agg backend')
#    mpl.use('Agg')
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')

from matplotlib import pyplot as plt
import matplotlib.lines as lines
import glob
#import pandas as pd
from scipy.optimize import curve_fit
import numpy as np

def power_law(x, a, b):
    return b*np.power(x, 1)

list_files =glob.glob("ttc_log_dump/*log")
#file_handle = open("ttc_log_dump/ttc_log_dump_file_timming.raw", "r")
trigger_interval = []
trigger_frequency = []
nentries = []
nentries_average = []
nentries_lenght = []
nentries_variance = []
ndrains = []
time_per_read_average = []
time_between_read_average = []
delta_t = [] # N / f
NR_Nt = [] # N / f

# no of FIFO drains
#col3 = []
#col4 = []
#elapsed_time (s) :         6.31484
#entries produced :     393222
#entries in log :       393222
# of orbits produced : 21984
# of orbits in log :   21984
#trigger interval :     600
#nentries :     131074  131074  131074

for xx in list_files:
  print(xx)
  file_handle = open(xx.replace(".raw", ".log"), "r")
  for yy in file_handle:
  #  print(xx, float(xx.split()[1]))
    if "no of FIFO drains" in yy :
        ndrains = ndrains + [int(yy.split(":")[1])]
    if "trigger rate (kHz)" in yy :
        #trigger_interval = trigger_interval + [float(yy.split(":")[1])]
        #trigger_frequency = trigger_frequency + [40000./float(yy.split(":")[1])]
        trigger_frequency = trigger_frequency  + [float(yy.split(":")[1])]
        trigger_interval = trigger_interval + [40000./float(yy.split(":")[1])]
    if "nentries "in yy :
        list_nentries = yy.split(":")[1].split()
        #list_nentries = list(filter(lambda x: x != '131074', list_nentries))
        #nentries = []
        #for ii, zz in enumerate(list_nentries) :
        nentries = [int(zz) for ii, zz in enumerate(list_nentries) if (ii > len(list_nentries)/10) ] # if (ii > 11 or len(list_nentries) < 13)
        #print (list_nentries)
        nentries_average = nentries_average + [sum(nentries)/len(nentries) if len(nentries) > 0 else 131074]
        nentries_lenght = nentries_lenght + [len(list_nentries)]
    if "elapsed time_between_reads(us)" in yy :
        list_nentries = yy.split(":")[1].split()
        time_between_read = [float(zz) for ii, zz in enumerate(list_nentries) if (ii > len(list_nentries)/10) ]
        time_between_read = time_between_read[:len(time_between_read)-1]
        #print(time_per_read)
        time_between_read_average += [sum(time_between_read)/len(time_between_read) if len(time_between_read) > 0 else 0]
    if "elapsed time_per_read(us)" in yy :
        list_nentries = yy.split(":")[1].split()
        time_per_read = [float(zz) for ii, zz in enumerate(list_nentries) if (ii > len(list_nentries)/10) ]
        time_per_read = time_per_read[:len(time_per_read)-1]
        #print(time_per_read)
        time_per_read_average += [sum(time_per_read)/len(time_per_read) if len(time_per_read) > 0 else 0]

print(trigger_frequency)

## delta_t
print("len delta_t", len(trigger_frequency), len(nentries_average))
for ee, entry in enumerate(nentries_average) :
    delta_t += [float(entry/trigger_frequency[ee])]
    NR_Nt += [entry/(trigger_frequency[ee]*1.3)]

nentries_average_to_fit = nentries_average
trigger_frequency_to_fit = trigger_frequency
trigger_frequency_to_plot = trigger_frequency

nentries_average_to_fit = list(filter(lambda x: x != 131074, nentries_average_to_fit))
shift = 7
n_to_remove = len(nentries_average_to_fit)-shift

nentries_average_to_fit = nentries_average_to_fit[:n_to_remove]
trigger_frequency_to_fit = trigger_frequency_to_fit[:n_to_remove] #+ trigger_frequency_to_fit[index+1 :]

print(len(nentries_average_to_fit), len(trigger_frequency_to_fit), n_to_remove, shift)
print(len(nentries_average), len(trigger_frequency))



#print(time_per_read_average)
#print(nentries_average)
#print(nentries_lenght)
#print("ndrains", ndrains)
#  col = col + [float(xx.split()[1])/1e-06]

#df = pd.DataFrame()
#df['trigger_interval'] = trigger_interval
#df['nentries_average'] = nentries_average

# plotting the points
#plt.xlim([0, 15.0])
#plt.hist(col, bins=500, alpha=0.5, label='full online decoding', density=1)
fig = plt.figure()
plt.plot(trigger_interval, nentries_average, marker='.', linestyle='dashed', linewidth = 0, markersize=12)
plt.plot([0.01, 60000], [131074.0, 131074.0], linestyle='dashed', linewidth = 1, markersize=0)
#fig.add_artist(lines.Line2D([600, 60000], [131074.0, 131074.0]))
plt.xscale('log')
plt.yscale('log')
plt.title('ramp ')
plt.ylabel( 'average # entries per FIFO drain', fontsize=15)
plt.xlabel('trigger interval', fontsize=15)
#plt.legend(loc='upper right')
plt.savefig("ttc_log_dump_file_timming.png")
print("saved ttc_log_dump_file_timming.png")
plt.figure().clear()


fig = plt.figure()
#print(trigger_frequency, nentries_average)
plt.plot(trigger_frequency, nentries_average, marker='.', linestyle='dashed', linewidth = 0, markersize=12)
plt.plot([10, 100000], [131074.0, 131074.0], linestyle='dashed', linewidth = 1, markersize=0)
########################
pars, cov = curve_fit(f=power_law, xdata=trigger_frequency_to_fit, ydata=nentries_average_to_fit, p0=[0, 0]) # , bounds=(-np.inf, np.inf)
plt.plot(trigger_frequency, power_law(trigger_frequency, *pars), linestyle='--', linewidth=1, color='r')
plt.plot(trigger_frequency_to_plot, nentries_average, marker='.', linestyle='dashed', linewidth = 0, markersize=12, color='r', label="slope = " + str(round(pars[1],2)) )
plt.plot(trigger_frequency_to_fit, nentries_average_to_fit, marker='.', linestyle='dashed', linewidth = 0, markersize=12, color='g')

plt.plot([trigger_frequency[n_to_remove+shift], trigger_frequency[n_to_remove+shift]], [1.0, 131074.0], linestyle='solid', linewidth = 1, markersize=0, color='r')
plt.plot([trigger_frequency[n_to_remove], trigger_frequency[n_to_remove]], [1.0, 131074.0], linestyle='dotted', linewidth = 1, markersize=0, color='r')
########################
#fig.add_artist(lines.Line2D([600, 60000], [131074.0, 131074.0]))
plt.xscale('log')
plt.yscale('log')
plt.title('ramp ')
plt.ylabel( 'average # entries per FIFO drain', fontsize=15)
plt.xlabel('trigger rate (kHz)', fontsize=15)
plt.legend(loc='upper left')
plt.savefig("ttc_log_dump_file_timming_in_frequency.png")
print("saved ttc_log_dump_file_timming_in_frequency.png")
plt.figure().clear()

## plot delta t
#print("delta_t", delta_t)
#print("trigger_frequency", trigger_frequency)
fig = plt.figure()
plt.plot(trigger_frequency, delta_t, marker='.', linestyle='dashed', linewidth = 0, markersize=12, label="delta t (us)")
plt.plot(trigger_frequency, NR_Nt, marker='.', linestyle='dashed', linewidth = 0, markersize=12, label="NR/Nt")
plt.plot([trigger_frequency[n_to_remove+shift], trigger_frequency[n_to_remove+shift]], [1.0, 131074.0], linestyle='solid', linewidth = 1, markersize=0, color='r')
plt.plot([trigger_frequency[n_to_remove], trigger_frequency[n_to_remove]], [1.0, 131074.0], linestyle='dotted', linewidth = 1, markersize=0, color='r')
plt.xscale('log')
#plt.yscale('log')
plt.title('not introducing delay ')
plt.ylabel( ' ', fontsize=15)
plt.xlabel('trigger rate (kHz)', fontsize=15)
plt.legend(loc='upper left')
plt.ylim(top=3, bottom=0)
#plt.xlim(right=trigger_frequency[n_to_remove])
plt.savefig("ttc_log_dump_delta_t.png")
plt.figure().clear()


fig = plt.figure()
plt.plot(trigger_interval, ndrains, marker='.', linestyle='dashed', linewidth = 0, markersize=12)
#plt.plot([10, 1000], [131074.0, 131074.0], linestyle='dashed', linewidth = 1, markersize=0)
#fig.add_artist(lines.Line2D([600, 60000], [131074.0, 131074.0]))
plt.xscale('log')
plt.yscale('log')
plt.title('ramp ')
plt.ylabel( 'number of FIFO drains', fontsize=15)
plt.xlabel('trigger interval', fontsize=15)
#plt.legend(loc='upper right')
plt.savefig("ttc_log_dump_file_ndrains.png")
plt.figure().clear()

fig = plt.figure()
#plt.plot(trigger_frequency[:n_to_remove], time_per_read_average[:n_to_remove], marker='.', linestyle='dashed', linewidth = 0, markersize=12, label="time to read")
#plt.plot(trigger_frequency[:n_to_remove], time_between_read_average[:n_to_remove], marker='.', linestyle='dashed', linewidth = 0, markersize=12, label="time between reads")
plt.plot(trigger_frequency, time_per_read_average, marker='.', linestyle='dashed', linewidth = 0, markersize=12, label="time to read")
plt.plot(trigger_frequency, time_between_read_average, marker='.', linestyle='dashed', linewidth = 0, markersize=12, label="time between reads")

#plt.plot([10, 1000], [131074.0, 131074.0], linestyle='dashed', linewidth = 1, markersize=0)
#fig.add_artist(lines.Line2D([600, 60000], [131074.0, 131074.0]))
plt.xscale('log')
#plt.yscale('log')
plt.title('not introducing delay ')
plt.ylabel( 'time per read average (us)', fontsize=15)
plt.xlabel('trigger rate (kHz)', fontsize=15)
plt.legend(loc='upper left')
plt.savefig("ttc_log_dump_file_time_per_read.png")
plt.figure().clear()
