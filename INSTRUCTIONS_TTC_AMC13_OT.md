# Setup 

Clone all the necessary software in the correct branches, all in the same folder:

```
git clone --recurse-submodules https://gitlab.cern.ch/cms_tk_ph2/TTC_FC7_Control.git
cd TTC_FC7_Control
source setup.sh
mkdir build ; cd build
cmake ..
make -j X
```

```
git clone -b amc_clock_patch --recurse-submodules --recursive ssh://git@gitlab.cern.ch:7999/mersi/Ph2_ACF.git Ph2_ACF_forkStefano
cd Ph2_ACF_forkStefano
source setup.sh
mkdir build ; cd build
cmake ..
make -j X
```

Or in `xtaldaq@ph2acf-02` go to:

```
cd /home/xtaldaq/TTC_FC7_developing/TTC_FC7_Control/ 
```

Source setup:

```
source setup.sh
source eudaq_setup.sh  # this will also source Ph2_ACF with the additional commands needed for this recipe
```

# Test steps 

## 1) reload the FW

- The firmware to use on the 2S is [here](https://udtc-ot-firmware.web.cern.ch/2s_8m_cic2_l12octa_l8dio5_withbend_nostubdebug_notlu/d19c_2s_8m_cic2_l12octa_l8dio5_withbend_nostubdebug_notlu.bin). It is the lates one merged to Dev by Wigner side.
- The firmware to use in the TTC-FC7 is here ADD A LINK FOR A PLACE TO DOWNLOAD!!! 

- At CERN ht modules are: 
    - 2S module (`"chtcp-2.0://ph2acf-02:10203?target=192.168.0.71:50001"`) 
    - TTC-FC7 (`"chtcp-2.0://ph2acf-02:10203?target=192.168.0.76:50001"`)

And the firmware in which one is are named as the commands to reload the firmware bellow point

```
fpgaconfig_TTC_FC7 -i 2021.09.10_veszpv_v17.bit -c ../settings/TTC_FC7Description_skeleton.xml
fpgaconfig -i 2s_cic1_2022-09-12 -c ../settings/2S_Module.xml
```

### On the setting in the xml 

The 2S module xml correct settings for the OT module is commited on thie TTC_FC7_Control repository [here](https://gitlab.cern.ch/cms_tk_ph2/TTC_FC7_Control/-/blob/master/settings/2S_Module.xml).

But if you want to use the default `2S_Module.xml`, from Stefano's fork, the modifications required on the settings are:

```
<Register name="ttc_enable"> 1 </Register>
<Register name="clock_source_u7">0</Register>
<Register name="trigger_source"> 1 </Register>
```

And, if you are not using the CERN hardware, the connection pointers to control hub ;-)

## 2) Do the AMC13 initialization, in a separate terminal 

In `xtaldaq@ph2acf-02` amc13 is installed on:

```
cd ~/TTC_FC7_developing/amc13-uhal-2.8/tools/bin/

```

To launch the executable:

```
source ../../env.sh
AMC13Tool2.exe -c ../../amc13/etc/amc13/connections.xml
```

To reset it, tn the executable:

```
ws 0x0 0x10
wv 0x0 0x0
```

- Warning: the second command will usually give an error `Caught microHAL exception... ControlHub returned error code 3 = 'no reply to status packet'`. We just run again and again up to work.

P.S.: Have a succesfull recipe how to install amc13

## 3) configure and test tts state on TTC-FC7

```
mkdir test_folder
cd test_folder

ttc_fc7_module_test -f ../settings/TTC_FC7Description_skeleton.xml --configure
```

- There is a bug in the firmware in locking the TTS state, right now this is being solved scanning the phase delay up to get a locked state, that is done in the step above.
    - WARNING: The fix in the software stop working 14/09/2022 (this has nothing to do with any change on the configuration made since last version of these instructions)
        - If the TTS does not lock you will need to repeat steps 1-3 in this order up to load (Work in progress for Xanda to understand why it stop working)
        - Changes wrt to last instructions: The setting the FMC source is now part of the configuration step

- Warning: In this step we set the trigger source fourcefully to 0 (zero). The trigger source should be set to 0 (none) before configuring the AMC13 otherwhise the TTS state  desistabilize to 15 (UNDEF). 
    - That is an additional feature of the FW


## 4) configure AMC13 (still in the executable, in a separate terminal)

```
wv 0x8 0x000000004		 # BX offset
wv 0x0000001c 0x30000000 # Trigger rule
wv 0x3 0x8000000		 # ignore DAQ data (TTS still working)
en 1-12                  # physical slot of the TTC-FC7 board 
```

## 5) configure OT-FC7 

```
ot_module_test -f ../settings/2S_Module.xml --reconfigure -b
```

TODO: test if the classic configuration on Ph2_ACF with minimal modifications would be equivalent.

## 6) synchronize orbit structure

```
ttc_fc7_module_test -f ../settings/TTC_FC7Description_skeleton.xml --sync-orbit
```

## 7.1) Send X ipbus triggers 

The trigger source is set in this step. 
The trigger settings are in [here](https://gitlab.cern.ch/cms_tk_ph2/TTC_FC7_Control/-/blob/master/settings/TTC_FC7Description_skeleton.xml#L156-159).
- If you chose as IPbus and also a number `nTriggersIPBus` the bellow will enable trigger and send the required number of IPBus triggers.

```
ttc_fc7_module_test -f ../settings/TTC_FC7Description_skeleton.xml --ipbus-send
```


You can then further send more 5 ipbus triggers with the command.

```
ttc_fc7_module_test -f ../settings/TTC_FC7Description_skeleton.xml --ipbus-send
```

## 7.2) Send start/stop continuous triggering 

The trigger source is set in this step. 
The trigger settings are in [here](https://gitlab.cern.ch/cms_tk_ph2/TTC_FC7_Control/-/blob/master/settings/TTC_FC7Description_skeleton.xml#L156-159).
- If you chose as external or user-defined trigger the command bellow will set the source and enable trigger.


```
ttc_fc7_module_test -f ../settings/TTC_FC7Description_skeleton.xml --set-trg
```

To disable the trigger, read a FIFO with:

```
ttc_fc7_module_test -f ../settings/TTC_FC7Description_skeleton.xml --log-dump
```

## Read the trigger count on the different systems

Read the trigger count on the TTC-FC7

```
ttc_fc7_module_test -f ../settings/TTC_FC7Description_skeleton.xml --trg-cnt
```

To continuoslly monitor the trigger count trigger count on the OT-FC7 you do (in a separate terminal)

```
ot_trigger_monitor -f ../settings/2S_Module.xml
```

Read the status in the AMC13 (in the executable)

```
st
```

## Checking status ttc side (to be done in intermediate steps, if necessary)

To directly check the TTS state, the trigger source (among other things) you can do. 
The configuration of what would it print out is [here](https://gitlab.cern.ch/cms_tk_ph2/TTC_FC7_Control/-/blob/master/settings/TTC_FC7Description_skeleton.xml#L142-149)

```
ttc_fc7_module_test -f ../settings/TTC_FC7Description_skeleton.xml --systemChecks
```

only the status of the trigger you can check with

```
ttc_fc7_module_test -f ../settings/TTC_FC7Description_skeleton.xml --trg-stat
```

## Plan to eudaq flow

- steps 2-5 in `eudaqproducer_TTC_FC7` init, pending: 
    - tts state bug to be fixed on the FW to be stable, or a in-line fix is made
    - AMC13 is linked to it
- add step 6 in  `eudaqproducer` configure
- steps 7 in `eudaqproducer_TTC_FC7` configure