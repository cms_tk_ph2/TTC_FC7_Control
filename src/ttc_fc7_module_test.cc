#include "../Utils/Timer.h"
#include "../Utils/argvparser.h"
#include "../Utils/easylogging++.h"
#include "../Utils/argvparser.h"
//#include "../tools/CalibrationExampleSimple.h"
#include "../System/TTC_FC7Controller.h"
#include "TApplication.h"
#include "TROOT.h"
#include <cstring>

using namespace TTC_FC7_HwDescription;
using namespace TTC_FC7_HwInterface;
using namespace TTC_FC7_System;
using namespace CommandLineProcessing;

INITIALIZE_EASYLOGGINGPP

int main(int argc, char* argv[])
{
    // configure the logger
    el::Configurations conf(std::string(std::getenv("TTC_FC7_BASE_DIR")) + "/settings/logger.conf");
    el::Loggers::reconfigureAllLoggers(conf);

    ArgvParser cmd;

    // init
    cmd.setIntroductoryDescription("CMS TTC_FC7 calibration example");
    // error codes
    cmd.addErrorCode(0, "Success");
    cmd.addErrorCode(1, "Error");
    // options
    cmd.setHelpOption("h", "help", "Print this help page");

    cmd.defineOption("file", "Hw Description File . Default value: settings/Calibration8CBC.xml", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("file", "f");

    cmd.defineOption("output", "Output Directory . Default value: Results", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("output", "o");

    cmd.defineOption("batch", "Run the application in batch mode", ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("batch", "b");

    cmd.defineOption("fullRun", "Initialise, configure, send triggers (if xml asks), save and decode data (if xml asks)");

    cmd.defineOption("initialize", "Initialise board");

    cmd.defineOption("configure", "Configure board");

    cmd.defineOption("reset", "Reset board");

    cmd.defineOption("log-dump", "Empty the FIFO and print it in the terminal");

    cmd.defineOption("trg-stat", "Read trigger status");

    cmd.defineOption("set-trg", "Set trigger");

    cmd.defineOption("sync-orbit", "Starting orbit structure");

    cmd.defineOption("ipbus-send", "Send 5 ipbuf triggers");

    cmd.defineOption("reset-triggers", "Se FMC as xml and force trigger source to 0 (none)");

    cmd.defineOption("trg-cnt", "Counting triggers");

    cmd.defineOption("version", "Print firmware version");
    cmd.defineOptionAlternative("version", "v");

    cmd.defineOption("statusCrt", "Print out clock frequencies");

    cmd.defineOption("systemChecks", "Print out all the system checks that are set in the xml");


    int result = cmd.parse(argc, argv);

    if(result != ArgvParser::NoParserError)
    {
        LOG(INFO) << cmd.parseErrorDescription(result);
        exit(1);
    }

    // now query the parsing results
    std::string cHWFile    = (cmd.foundOption("file")) ? cmd.optionValue("file") : "settings/Calibration8CBC.xml";

    // create a TTC_FC7Controller Object, I can then construct all other tools from that using the Inherit() method
    // this tool stays on the stack and lives until main finishes - all other tools will update the HWStructure from
    // cTool
    TTC_FC7Controller              cTool;
    std::stringstream outp;
    cTool.InitializeHw(cHWFile, outp);
    cTool.InitializeSettings(cHWFile, outp);
    LOG(INFO) << outp.str();
    outp.str("");

    if (cmd.foundOption("fullRun") || cmd.foundOption("configure"))
    {
      cTool.ConfigureHw();
      cTool.configure();
      //cTool.reset_triggers();
      cTool.scan_tts_idelay();
      cTool.reset_triggers();
    }

    if (cmd.foundOption("fullRun") || cmd.foundOption("reset-triggers"))
    {
      cTool.reset_triggers();
    }

    if (cmd.foundOption("reset")) cTool.Reset();

    if (cmd.foundOption("fullRun") == true || cmd.foundOption("version")) cTool.print_version();

    if (cmd.foundOption("statusCrt")) cTool.status_crt();

    if (cmd.foundOption("systemChecks")) cTool.system_checks();

    if (cmd.foundOption("log-dump")) cTool.ttc_log_dump_offline_decoding(2, true);

    if (cmd.foundOption("trg-stat")) cTool.read_trigger_enable_state();

    if (cmd.foundOption("set-trg")) 
    {
      int trigger_interval = cTool.findValueInSettings("Trigger_rate_UserDefFreq");
      cTool.send_trigger(trigger_interval);
    }

    if (cmd.foundOption("sync-orbit")) cTool.sync_orbit();

    if (cmd.foundOption("ipbus-send")) cTool.send_N_ipbus_triggers(5);
    // send 2 sec of user-defined freq trigger
    
    if (cmd.foundOption("trg-cnt")) cTool.status_cnt();

    if (cmd.foundOption("fullRun")){
      bool send_triggers  = cTool.findValueInSettings("send_triggers");
      bool termination_and_test  = cTool.findValueInSettings("termination_and_test");
      bool ramp_trigger_interval = cTool.findValueInSettings("ramp_trigger_interval");
      int  TriggerSource = cTool.findValueInSettings("TriggerSource"); // enter as global variable
      bool rst = cTool.findValueInSettings("rst");

      if ( rst ) cTool.Reset();

      std::vector<int> ramp_trigger_rate_list;
      if ( !ramp_trigger_interval || TriggerSource == 1 )
      {
          int trigger_interval = cTool.findValueInSettings("Trigger_rate_UserDefFreq");
          ramp_trigger_rate_list =  {trigger_interval};
      } else
      {
          ramp_trigger_rate_list = {7, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2200, 2400, 2600, 2800, 3000, 3500, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 12000, 14000, 16000};
      }
      for (int const&  xx : ramp_trigger_rate_list)
      {
          cTool.ConfigureHw();
          cTool.print_version();

          cTool.configure();
          cTool.system_checks();

          if ( termination_and_test ) cTool.termination_and_test();
          if ( send_triggers )
          {
            cTool.send_trigger(xx);
            cTool.ttc_log_dump_offline_decoding(xx);
          }

      }
    }

    cTool.Start(0);
    cTool.Stop();

    //if(!batchMode) cApp.Run();
    return 0;
}
