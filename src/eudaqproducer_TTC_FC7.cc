﻿#include "../Utils/Utilities.h"
#include "../Utils/argvparser.h"
#include "../tools/Eudaq2Producer_TTC_FC7.h"

using namespace TTC_FC7_HwDescription;
using namespace TTC_FC7_HwInterface;
using namespace TTC_FC7_System;
using namespace CommandLineProcessing;
INITIALIZE_EASYLOGGINGPP

int main(int argc, char** argv)
{
    // configure the logger
    el::Configurations conf(std::string(std::getenv("TTC_FC7_BASE_DIR")) + "/settings/logger.conf");
    el::Loggers::reconfigureAllLoggers(conf);
    ArgvParser cmd;

    // init
    cmd.setIntroductoryDescription("CMS TTC_FC7_Control EUDAQ Producer for Test Beam Operations");
    // error codes
    cmd.addErrorCode(0, "Success");
    cmd.addErrorCode(1, "Error");

    // options
    cmd.setHelpOption("h", "help", "Print this help page");
    cmd.defineOption("runcontrol", "The RunControl address. Default value: tcp://localhost:44000", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("runcontrol", "r");
    cmd.defineOption("name", "Producer code name. Default value: ttcfc7producer", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("name", "n");
    cmd.defineOption("save", "Save the data to a raw file.  ", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("save", "s");

    // parse
    int result = cmd.parse(argc, argv);
    if(result != ArgvParser::NoParserError)
    {
        LOG(INFO) << cmd.parseErrorDescription(result);
        exit(1);
    }

    // get values
    std::string cRunControlAddress = (cmd.foundOption("runcontrol")) ? cmd.optionValue("runcontrol") : "tcp://localhost:44000";
    std::string cName              = (cmd.foundOption("name")) ? cmd.optionValue("name") : "ttcfc7producer";
    std::string cOutputFile;

// create eudaq2 producer (if need can also create eudaq1 here)
#ifdef __EUDAQ__
    LOG(INFO) << "Trying to connect to RunControl";
    Eudaq2Producer_TTC_FC7 cProducer(cName, cRunControlAddress);
    ///*
    try
    {
        LOG(INFO) << "Trying to connect to RunControl";
        //cProducer.Destroy();
        cProducer.Connect();
    }
    catch(...)
    {
        LOG(ERROR) << "Can not connect to RunControl at " << cRunControlAddress.c_str();
        return -1;
    }
    //*/
    LOG(INFO) << "Connected";

    while(cProducer.IsConnected()) { std::this_thread::sleep_for(std::chrono::seconds(1)); }
#endif

    // as well damn if u want, cdz
    //cProducer.Destroy();
    return 0;
}
