#include <cstdlib>
#include <inttypes.h>
#include <string>
#include <vector>

// #include "../HWDescription/BeBoard.h"
// #include "../HWInterface/BeBoardInterface.h"
#include "../System/TTC_FC7Controller.h"
#include "../Utils/ConsoleColor.h"
#include "../Utils/argvparser.h"

using namespace TTC_FC7_HwDescription;
using namespace TTC_FC7_HwInterface;
using namespace TTC_FC7_System;
using namespace CommandLineProcessing;

INITIALIZE_EASYLOGGINGPP

void verifyImageName(const std::string& strImage, const std::vector<std::string>& lstNames)
{
    if(lstNames.empty())
    {
        if(strImage.compare("1") != 0 && strImage.compare("2") != 0)
        {
            LOG(ERROR) << "Error, invalid image name, should be 1 (golden) or 2 (user)";
            exit(1);
        }
    }
    else
    {
        bool bFound = false;

        for(size_t iName = 0; iName < lstNames.size(); iName++)
        {
            if(!strImage.compare(lstNames[iName]))
            {
                bFound = true;
                break;
            }
        }

        if(!bFound)
        {
            LOG(ERROR) << "Error, this image name: " << strImage << " is not available on SD card";
            exit(1);
        }
    }
}



int main(int argc, char* argv[])
{
    auto* baseDirChar_p = std::getenv("TTC_FC7_BASE_DIR");
    if(baseDirChar_p == nullptr)
    {
        LOG(ERROR) << "Error, the environment variable TTC_FC7_BASE_DIR is not initialized (hint: source setup.sh)";
        exit(1);
    }

    el::Configurations conf(std::string(baseDirChar_p) + "/settings/logger.conf");
    el::Loggers::reconfigureAllLoggers(conf);

    TTC_FC7Controller cTTC_FC7Controller;

    ArgvParser       cmd;

    cmd.setIntroductoryDescription("CMS TTC_FC7  Data acquisition test and Data dump");

    cmd.addErrorCode(0, "Success");
    cmd.addErrorCode(1, "Error");

    cmd.setHelpOption("h", "help", "Print this help page");

    cmd.defineOption("list", "Print the list of available firmware images on SD card (works only with CTA boards)");
    cmd.defineOptionAlternative("list", "l");

    cmd.defineOption("delete", "Delete a firmware image on SD card (works only with CTA boards)", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("delete", "d");

    cmd.defineOption("file", "Local FPGA Bitstream file (*.mcs format for GLIB or *.bit/*.bin format for CTA boards)", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("file", "f");

    cmd.defineOption("download", "Download an FPGA configuration from SD card to file (only for CTA boards)", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("download", "o");

    cmd.defineOption("config", "Hw Description File . Default value: settings/HWDescription_2CBC.xml", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/);
    cmd.defineOptionAlternative("config", "c");

    cmd.defineOption("image", "Without -f: load image from SD card to FPGA\nWith    -f: name of image written to SD card\n-f specifies the source filename", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("image", "i");

    cmd.defineOption("board", "In case of multiple boards in the same file, specify board Id", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("board", "b");

    int result = cmd.parse(argc, argv);

    if(result != ArgvParser::NoParserError)
    {
        LOG(INFO) << cmd.parseErrorDescription(result);
        exit(1);
    }

    std::string        cHWFile = (cmd.foundOption("config")) ? cmd.optionValue("config") : "settings/PS_HalfModule.xml";
    std::ostringstream cStr;
    cTTC_FC7Controller.InitializeHw(cHWFile, cStr);
    std::vector<std::string> lstNames = cTTC_FC7Controller.fTTC_FC7Interface->getFpgaConfigList();
    std::string              cFWFile;
    std::string              strImage("1");

    if(cmd.foundOption("list"))
    {
        LOG(INFO) << lstNames.size() << " firmware images on SD card:";

        for(auto& name: lstNames) LOG(INFO) << " - " << name;

        exit(0);
    }
    else if(cmd.foundOption("file"))
    {
        cFWFile = cmd.optionValue("file");

        if(lstNames.size() == 0 && cFWFile.find(".mcs") == std::string::npos)
        {
            LOG(ERROR) << "Error, the specified file is not a .mcs file";
            exit(1);
        }
        else if(lstNames.size() > 0 && cFWFile.compare(cFWFile.length() - 4, 4, ".bit") && cFWFile.compare(cFWFile.length() - 4, 4, ".bin"))
        {
            LOG(ERROR) << "Error, the specified file is neither a .bit nor a .bin file";
            exit(1);
        }
    }
    else if(cmd.foundOption("delete") && !lstNames.empty())
    {
        strImage = cmd.optionValue("delete");
        verifyImageName(strImage, lstNames);
        cTTC_FC7Controller.fTTC_FC7Interface->DeleteFpgaConfig(strImage);
        LOG(INFO) << "Firmware image: " << strImage << " deleted from SD card";
        exit(0);
    }
    else if(!cmd.foundOption("image"))
    {
        cFWFile = "";
        LOG(ERROR) << "Error, no FW image specified";
        exit(1);
    }

    if(cmd.foundOption("image"))
    {
        strImage = cmd.optionValue("image");

        if(!cmd.foundOption("file"))
        {
            verifyImageName(strImage, lstNames);
            LOG(INFO) << BOLDBLUE << ">>> Done <<<" << RESET;
        }
    }
    else if(!lstNames.empty())
        strImage = "GoldenImage.bin";

    if(!cmd.foundOption("file") && !cmd.foundOption("download"))
    {
        cTTC_FC7Controller.fTTC_FC7Interface->JumpToFpgaConfig(strImage);
        exit(0);
    }

    bool cDone = 0;

    if(cmd.foundOption("download"))
        cTTC_FC7Controller.fTTC_FC7Interface->DownloadFpgaConfig(strImage, cmd.optionValue("download"));
    else
        cTTC_FC7Controller.fTTC_FC7Interface->FlashProm(strImage, cFWFile.c_str());

    uint32_t progress;

    while(cDone == 0)
    {
        progress = cTTC_FC7Controller.fTTC_FC7Interface->GetConfiguringFpga()->getProgressValue();

        if(progress == 100)
        {
            cDone = 1;
            LOG(INFO) << BOLDBLUE << ">>> 100% Done <<<" << RESET;
        }
        else
        {
            LOG(INFO) << progress << "%  " << cTTC_FC7Controller.fTTC_FC7Interface->GetConfiguringFpga()->getProgressString() << "                 \r" << std::flush;
            sleep(1);
        }
    }
}
