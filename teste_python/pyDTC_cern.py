#!/usr/bin/env python3

#----------------------------------------------------------------------------------
#-- file pyDTC.py
#----------------------------------------------------------------------------------
#-- authors : Tamas Balazs (tamas.balazs@cern.ch)
#--           Viktor Vesypremi (viktor.veszpremi@cern.ch)
#--	      Lajos Palanki (LP618@ic.ac.uk)
#--	      Alexander Ruede (aruede@cern.ch)
#----------------------------------------------------------------------------------
#-- version 6.0
#--
#-- Details :
#-- Subrutins for TTC-uTTC-firmware python library.
#--
#-- Last changes:
#-- 29-05-2020
#---------------------------------------------------------------------------------

import sys
import uhal
from time import sleep

class pyDTC:

    def __init__(self, ipaddr):
        uhal.setLogLevelTo(uhal.LogLevel.WARNING)
        self.ipaddr = ipaddr
        # udp transactions worked before CentOS 7.7 upgrade only !
        # self.hw = uhal.getDevice("FC7", "ipbusudp-2.0://" + self.ipaddr + ":50001", "file://./cfg/device_address_table_fc7.xml")
        self.hw = uhal.getDevice("FC7", "chtcp-2.0://ph2acf-02:10203?target=" + self.ipaddr + ":50001", "file://./../settings/address_tables/device_address_table_fc7.xml")

    # ---- IPbus ------------------------------------#

    def ipb_read(self, nodeID, dispatch=1):
        word = self.hw.getNode(nodeID).read()
        if dispatch == 1:
            self.hw.dispatch()
        return int(word)

    def ipb_write(self, nodeID, data, dispatch=1):
        self.hw.getNode(nodeID).write(data)
        if dispatch == 1:
            self.hw.dispatch()
        return int(data)

    def ipb_dispatch(self):
        return self.hw.dispatch()

    #------------------------------------------------#

    # ---- reset and powering FMCs ------------------#

    def l12_pwr_on(self):
        self.ipb_write("system.reg_ctrl_2.fmc_l12_pwr_en",1,1)
        self.ipb_write("system.reg_ctrl_2.fmc_pg_c2m",1,1)
        print('L12 FMC power ON')

    def l8_pwr_on(self):
        self.ipb_write("system.reg_ctrl_2.fmc_l8_pwr_en",1,1)
        self.ipb_write("system.reg_ctrl_2.fmc_pg_c2m",1,1)
        print('L8 FMC power ON')

    def fmc_pwr_on(self):
        self.ipb_write("system.reg_ctrl_2.fmc_l12_pwr_en",1,1)
        self.ipb_write("system.reg_ctrl_2.fmc_l8_pwr_en",1,1)
        self.ipb_write("system.reg_ctrl_2.fmc_pg_c2m",1,1)
        print('L8 & L12 FMC power ON')

    def fmc_pwr_off(self):
        self.ipb_write("system.reg_ctrl_2.fmc_l12_pwr_en",0,1)
        self.ipb_write("system.reg_ctrl_2.fmc_l8_pwr_en",0,1)
        self.ipb_write("system.reg_ctrl_2.fmc_pg_c2m",0,1)
        print('L8 & L12 FMC power OFF')

    def set_reset_all(self):
        self.ipb_write("user.ctrl_regs.reset_reg.global_rst",1,1)
        self.ipb_write("user.ctrl_regs.reset_reg.clk_gen_rst",1,1)

    def release_reset_all(self):
        self.ipb_write("user.ctrl_regs.reset_reg.global_rst",0,1)
        self.ipb_write("user.ctrl_regs.reset_reg.clk_gen_rst",0,1)
        sleep(0.4)

    def reset_setup(self):
        print("Resetting...")
        self.set_reset_all()
        sleep(0.2)
        self.release_reset_all()
        sleep(0.2)
        print("Reset DONE.")
        #sys.exit(1)

    # ---- Trigger ---------------------------------#

    def send_trigger(self, int, n = 1, delay = 1):
        print('Sending trigger')

        for i in range(n):
            self.ipb_write("user.ctrl_regs.fast_cmd_reg_1.ipb_trigger",1,1)
            print('Trigger ' + str(i) + ' sent')
            sleep(int)
            self.ipb_write("user.ctrl_regs.fast_cmd_reg_1.ipb_trigger",0,1)
            if (i == n-1):
                break
            sleep(delay)

    def start_trigger(self):
        self.ipb_write("user.ctrl_regs.trigger.trigger_enable",1,1)
        sleep(0.1)
        self.ipb_write("user.ctrl_regs.trigger.trigger_enable",0,1)
        print('Triggering started')

    def stop_trigger(self):
        self.ipb_write("user.ctrl_regs.fast_cmd_reg_1.stop_trigger",1,1)
        sleep(0.1)
        self.ipb_write("user.ctrl_regs.fast_cmd_reg_1.stop_trigger",0,1)
        print('Triggering stopped')

    def select_trigger(self, src):
        print ('Changing trigger to: ' + str(src))
        self.ipb_write("user.ctrl_regs.trigger.trigger_source",src,1)

    def read_source(self): # For debug
        print('The source is:' + str(self.ipb_read("user.stat_regs.trigger.trigger_source_o",1)))

    def read_state(self):  # For debug
        print('The state is:' + str(self.ipb_read("user.stat_regs.trigger.trigger_state",1)))

    def set_trigger_interval(self, interval):
        self.ipb_write("user.ctrl_regs.trigger_interval",interval,1)
        print("Trigger interval changed to:" + str(interval))

    def DIO5_configure(self, config):
        self.ipb_write("user.ctrl_regs.ext_tlu_1.dio5_ch_out_en",config,1)
        print("DIO5 output configured to:" + bin(config))

    def check_FIFO(self):
        while True:
            print("number of triggers", self.ipb_read("user.stat_regs.trigger_cntr",1), bin(self.ipb_read("user.stat_regs.trigger_cntr",1)))
            sleep(2)
            # event counter -- reset in the beginning of every orbit

    def DIO5_term_50_enable(self):

        self.ipb_write("user.ctrl_regs.ext_tlu_1.dio5_en",1,1)
        print("DIO5 enable")
        self.ipb_write("user.ctrl_regs.ext_tlu_1.dio5_ch_out_en",0,1)
        print("DIO5 configure -- it is from 15 to 17 in Tamas original program, remember the meaning")
        self.ipb_write("user.ctrl_regs.ext_tlu_1.dio5_term_50ohm_en",31,1)
        print("DIO5 termination in all")
        # 0 - no channel with termination
        # 1 - only l1
        # 2 - 1-5 chanel enable / +5 terminator enable in that channel
        # all of them 16 + 8 - 1 = 31

    def DIO5_set_threshold(self) :
        ch2_thr = self.ipb_read("user.ctrl_regs.ext_tlu_1.dio5_ch2_thr",1)
        print("------------------------------------------------")
        print('ch2_thr before setting it: ', str(ch2_thr))
        print("------------------------------------------------")

        THR = 90
        self.ipb_write("user.ctrl_regs.ext_tlu_1.dio5_ch1_thr",THR,1)
        self.ipb_write("user.ctrl_regs.ext_tlu_1.dio5_ch2_thr",THR,1)
        self.ipb_write("user.ctrl_regs.ext_tlu_2.dio5_ch3_thr",THR,1)
        self.ipb_write("user.ctrl_regs.ext_tlu_2.dio5_ch4_thr",THR,1)
        self.ipb_write("user.ctrl_regs.ext_tlu_2.dio5_ch5_thr",THR,1)

        ch2_thr = self.ipb_read("user.ctrl_regs.ext_tlu_1.dio5_ch2_thr",1)
        print("------------------------------------------------")
        print('ch2_thr reading after setting it (to test what the cpp is reading): ' + str(ch2_thr))
        print("------------------------------------------------")

    # ---- 8SFP FMC ---------------------------------#

    def set_reset_8SFP(self):
        self.ipb_write("user.ctrl_regs.reset_reg.global_rst",1,1)
        self.ipb_write("user.ctrl_regs.reset_reg.clk_gen_rst",1,1)
        #self.ipb_write("user.ctrl_regs.reset_reg.fmc_pll_rst",0,1) #INVERTED!
        #self.ipb_write("user.ctrl_regs.reset_reg.fmc_pll_rst",1,1)
        self.ipb_write("user.ctrl_regs.reset_reg.i2c_rst",1,1)
        print("Global reset & clock genenerator reset & 8SFP FMC i2c reset sent")

    def release_reset_8SFP(self):
        self.ipb_write("user.ctrl_regs.reset_reg.global_rst",0,1)
        self.ipb_write("user.ctrl_regs.reset_reg.clk_gen_rst",0,1)
        sleep(0.5)
        #self.ipb_write("user.ctrl_regs.reset_reg.fmc_pll_rst",1,1) #INVERTED!
        #self.ipb_write("user.ctrl_regs.reset_reg.fmc_pll_rst",0,1)
        sleep(0.5)
        self.ipb_write("user.ctrl_regs.reset_reg.i2c_rst",0,1)
        print("Global reset & clock genenerator reset & 8SFP FMC i2c reset released")

    def status_crt(self):
        sleep(3)
        clk_rate_1 = self.ipb_read("user.stat_regs.clk_rate_1",1)
        clk_rate_2 = self.ipb_read("user.stat_regs.clk_rate_2",1)
        clk_rate_3 = self.ipb_read("user.stat_regs.clk_rate_3",1)
        clk_rate_4 = self.ipb_read("user.stat_regs.clk_rate_4",1)
        clk_rate_5 = self.ipb_read("user.stat_regs.clk_rate_5",1)
        clk_rate_6 = self.ipb_read("user.stat_regs.clk_rate_6",1)
        clk_rate_7 = self.ipb_read("user.stat_regs.clk_rate_7",1)
        clk_rate_8 = self.ipb_read("user.stat_regs.clk_rate_8",1)
        clk_rate_9 = self.ipb_read("user.stat_regs.clk_rate_9",1)
        print("Measuring clocks in the firmware ...")
        print("------------------------------------------------")
        print("clk_rate_1 --> ipb_clk                   %s", clk_rate_1)
        print("clk_rate_2 --> fabric_clk        	%s", clk_rate_2)
        print("clk_rate_3 --> clk_40MHz                 %s", clk_rate_3)
        print("clk_rate_4 --> clk_80MHz                 %s", clk_rate_4)
        print("clk_rate_5 --> clk_160MHz                %s", clk_rate_5)
        print("clk_rate_6 --> osc125_a_mgtrefclk_i      %s", clk_rate_6)
        print("clk_rate_7 --> fmc_l12_pll_clk           %s", clk_rate_7)
        print("clk_rate_8 --> clk_200MHz                %s", clk_rate_8)
        print("clk_rate_9 --> ref_clk_200MHz            %s", clk_rate_9)
        print("------------------------------------------------")
        print("")

    #------------------------------------------------#

    # ---- i2c master -------------------------------#

    def i2c_reset(self):
        self.ipb_write("user.ctrl_regs.reset_reg.i2c_rst",1,1)
        sleep(0.1)
        #self.ipb_write("user.ctrl_regs.reset_reg.i2c_rst",0,1)
        print("8SFP i2c master reset sent")

    def i2c_enable(self):
        self.ipb_write("user.ctrl_regs.i2c_settings",0x0083e8,1)
        print("8SFP i2c master enabled")

    def i2c_disable(self):
        self.ipb_write("user.ctrl_regs.i2c_settings",0x000000,1)
        print("8SFP i2c master disabled")

    def i2c_command(self):
        self.ipb_write("user.ctrl_regs.i2c_command",0x00F40004 | 0x80000000,1)
        self.ipb_write("user.ctrl_regs.i2c_command",0x00F40004,1)
        fact = 1
        for i in range(1,1000):
            fact = fact * i
        self.ipb_write("user.ctrl_regs.i2c_command",0x00B80000 | 0x80000000,1)
        self.ipb_write("user.ctrl_regs.i2c_command",0x00B80000,1)
        self.ipb_dispatch()
        fact = 1
        for i in range(1,1000):
            fact = fact * i
        self.ipb_write("user.ctrl_regs.i2c_command",0x00F40000 | 0x80000000,1)
        self.ipb_write("user.ctrl_regs.i2c_command",0x00F40000,1)

    def i2c_reply(self):
        i2c_reply = self.ipb_read("user.ctrl_regs.i2c_reply",1)
        print("------------------------------------------------")
        print("i2c_reply        "), i2c_reply
        print("------------------------------------------------")
        print("")

    def i2c_status(self):
        i2c_settings = self.ipb_read("user.ctrl_regs.i2c_settings",1)
        i2c_command = self.ipb_read("user.ctrl_regs.i2c_command",1)
        i2c_reply = self.ipb_read("user.ctrl_regs.i2c_reply",1)
        reg_i2c_settings = self.ipb_read("user.ctrl_regs.reg_i2c_settings",1)
        print("------------------------------------------------")
        print("i2c_settings     %s", hex(i2c_settings))
        print("i2c_command      %s", hex(i2c_command))
        print("i2c_reply        %s", i2c_reply)
        print("reg_i2c_settings %s", reg_i2c_settings)
        print("------------------------------------------------")
        print("")

    #------------------------------------------------#

    # ---- DIO5 -------------------------------------#

    # switching clock source to fmc_l8_clk1 external clock DIO5 channel 5
    def clock_source(self, source):
        self.ipb_write("system.reg_ctrl.clock_source",source,1)
        print("Clock source switched to:" + bin(source))

    def enable_DIO5(self):
        self.ipb_write("user.ctrl_regs.ext_tlu_1.dio5_en",1,1)
        print("DIO5 enabled")

    def disable_DIO5(self):
        self.ipb_write("user.ctrl_regs.ext_tlu_1.dio5_en",0,1)
        print("DIO5 disabled")

    def enable_load_config_DIO5(self):
        self.ipb_write("user.ctrl_regs.ext_tlu_reg2.dio5_load_config",1,1)
        print("DIO5 enabled")

    def disable_load_config_DIO5(self):
        self.ipb_write("user.ctrl_regs.ext_tlu_reg2.dio5_load_config",0,1)
        print("DIO5 enabled")

    def channel_DIO5(self, config):
        self.ipb_write("user.ctrl_regs.ext_tlu_1.dio5_ch_out_en",config,1)
        print("DIO5 output configured to:" + bin(config))

    def threshold_channel1_DIO5(self, config):
        self.ipb_write("user.ctrl_regs.ext_tlu_1.dio5_ch1_thr",config,1)
        print("DIO5 channel 1 threshold configured to:" + bin(config))

    def status_DIO5(self):
        print("------------------------------------------------")
        imp_cnt = self.ipb_read("user.stat_regs.imp_cnt",1)
        print("------------------------------------------------")
        print("imp_cnt           ", imp_cnt)
        print("------------------------------------------------")
        print("")

    def ext_clk(self):
        self.ipb_write("user.ctrl_regs.trigger.ext_clk_en",1,1)

    def int_clk(self):
        self.ipb_write("user.ctrl_regs.trigger.ext_clk_en",0,1)

    def status_clk_gen(self):
        clk_40_locked_1 = self.ipb_read("user.stat_regs.status_clk_gen.clk_40_locked_1",1)
        clk_40_locked_2 = self.ipb_read("user.stat_regs.status_clk_gen.clk_40_locked_2",1)
        ref_clk_locked  = self.ipb_read("user.stat_regs.status_clk_gen.ref_clk_locked",1)
        print("------------------------------------------------")
        print("clk_40_locked_1   %s", clk_40_locked_1)
        print("clk_40_locked_2   %s", clk_40_locked_2)
        print("ref_clk_locked    %s", ref_clk_locked)
        print("------------------------------------------------")
        print("")

    #------------------------------------------------#

    # ---- L1A counters -----------------------------#

    def L1A_cnt(self):
        l1a_counter = self.ipb_read("user.stat_regs.l1a_counter",1)
        test_l1a_counter = self.ipb_read("user.stat_regs.test_l1a_counter",1)
        ext_l1a_counter = self.ipb_read("user.stat_regs.ext_l1a_counter",1)
        print("------------------------------------------------")
        print("L1A_counter       %s", l1a_counter)
        print("test_L1A_counter  %s", test_l1a_counter)
        print("ext_L1A_counter   %s", ext_l1a_counter)
        print("------------------------------------------------")
        print("")

    #------------------------------------------------#

    # ---- L1Aph_phase ------------------------------#

    def L1Aph_phase(self):
        L1Aph_phase = self.ipb_read("user.stat_regs.l1aph.phase",1)
        print("------------------------------------------------")
        print("L1Aph_phase (dec) %s", L1Aph_phase)
        print("L1Aph_phase (hex) %s", hex(L1Aph_phase))
        print("------------------------------------------------")
        print("")

    #------------------------------------------------#

    # ---- IDELAY -----------------------------------#

    def idelay_intap(self, intap):
        print ('Changing IDELAY intap to: ' + str(intap))
        self.ipb_write("user.ctrl_regs.idelay_control.intap",intap,1)

    def idelay_load(self):
        self.ipb_write("user.ctrl_regs.idelay_control.load",1,1)

    def idelay_ce(self):
        self.ipb_write("user.ctrl_regs.idelay_control.ce",1,1)

    def idelay_outtap(self):
        outtap = self.ipb_read("user.stat_regs.idelay_status.outtap",1)
        print("------------------------------------------------")
        print("IDELAY_outtap     "), outtap
        print("------------------------------------------------")
        print("")

    #------------------------------------------------#

    # ---- TTS --------------------------------------#

    def TTS_state(self):
        TTS_state = self.ipb_read("user.stat_regs.TTS_status.TTS_state",1)
        print("------------------------------------------------")
        print("TTS_state         ")
        print(TTS_state)
        print("------------------------------------------------")
        print("")

    #------------------------------------------------#

    # ---- BGo --------------------------------------#

    def send_bgo(self, bgo):
        print ('Sending BGo command:' + str(hex(bgo)))
        self.ipb_write("user.ctrl_regs.bgo.bgo_command",bgo,1)

    #------------------------------------------------#


    def version(self):
        sleep(0.1)
        usr_ver_major = self.ipb_read("user.stat_regs.usr_ver.usr_ver_major",1)
        print("------------------------------------------------")
        print('usr_ver_major = ', usr_ver_major)
        usr_ver_minor = self.ipb_read("user.stat_regs.usr_ver.usr_ver_minor",1)
        print('usr_ver_minor = ', usr_ver_minor)
        usr_ver_build = self.ipb_read("user.stat_regs.usr_ver.usr_ver_build",1)
        print('usr_ver_build = ', usr_ver_build)
        usr_firmware_dd = self.ipb_read("user.stat_regs.usr_ver.usr_firmware_dd",1)
        usr_firmware_mm = self.ipb_read("user.stat_regs.usr_ver.usr_firmware_mm",1)
        usr_firmware_yy = self.ipb_read("user.stat_regs.usr_ver.usr_firmware_yy",1)
        print("------------------------------------------------")
        print('usr_firmware_date dd/mm/yy = ' + str(usr_firmware_dd) + '/' + str(usr_firmware_mm) + '/'+  str(usr_firmware_yy))
        print("------------------------------------------------")
