#----------------------------------------------------------------------------------
#-- file bsic_prog.py
#----------------------------------------------------------------------------------
#-- authors : Tamas Balazs (tamas.balazs@cern.ch)
#--           Viktor Vesypremi (viktor.veszpremi@cern.ch)
#--	      Lajos Palanki (LP618@ic.ac.uk)
#--	      Alexander Ruede (aruede@cern.ch)
#----------------------------------------------------------------------------------
#-- version 6.0
#--
#-- Details :
#-- Arguments for TTC-uTTC-firmware python library.
#--
#-- Last changes:
#-- 29-05-2020
#---------------------------------------------------------------------------------

import sys
from pyDTC_cern import *
from time import sleep

# Create object, specifying the IP address
# and connecting to the hardware IP endpoint
uTTC = pyDTC( "192.168.0.76" )
sleep(0.2)

if len(sys.argv) > 1 and sys.argv[1] == "test_setup_external":
    uTTC.reset_setup()
    sleep (0.2)
    uTTC.DIO5_term_50_enable()
    uTTC.DIO5_set_threshold()
    sleep (0.2)
    print('Checking clock frequencies with internal clock')
    #uTTC.int_clk()
    uTTC.status_crt()
    print('Init external triggering/clock')
    uTTC.select_trigger(5) # Trigger source.   # 1=IPBus, 2=Test-FSM, 3=TTC, 4=TLU, 5=External, 6=Hit-Or, 7=User-defined frequency
    uTTC.start_trigger()
    uTTC.ext_clk()
    print('external triggering/clock complete')
    sleep(0.2)
    uTTC.status_clk_gen()
    sleep (0.2)
    print('Checking clock frequencies after setting external clock')
    uTTC.status_crt()
    uTTC.l12_pwr_on()
    sleep (0.2)
    # we do not know if the bellow is necessary
    uTTC.i2c_enable()
    uTTC.i2c_command()
    sleep (0.2)
    uTTC.read_source()
    uTTC.read_state()
    uTTC.check_FIFO()

if len(sys.argv) > 1 and sys.argv[1] == "test_setup_internal":
    uTTC.reset_setup()
    sleep (0.2)
    uTTC.DIO5_term_50_enable()
    uTTC.DIO5_set_threshold()
    sleep (0.2)
    print('Checking clock frequencies with internal clock')
    #uTTC.int_clk()
    uTTC.status_crt()
    print('Init external triggering/clock')
    uTTC.select_trigger(1) # Trigger source.   # 1=IPBus, 2=Test-FSM, 3=TTC, 4=TLU, 5=External, 6=Hit-Or, 7=User-defined frequency
    uTTC.start_trigger()
    #uTTC.ext_clk()
    print('external triggering/clock complete')
    sleep(0.2)
    uTTC.status_clk_gen()
    sleep (0.2)
    print('Checking clock frequencies after setting external clock')
    uTTC.status_crt()
    uTTC.l12_pwr_on()
    sleep (0.2)
    # we do not know if the bellow is necessary
    uTTC.i2c_enable()
    uTTC.i2c_command()
    sleep (0.2)
    uTTC.read_source()
    uTTC.read_state()
    uTTC.check_FIFO()

if len(sys.argv) > 1 and sys.argv[1] == "version":
    uTTC.version()

######################################
# 
######################################

if len(sys.argv) > 1 and sys.argv[1] == "configure":
    uTTC.fmc_pwr_on()
    uTTC.i2c_enable()
    uTTC.i2c_command()
    uTTC.enable_DIO5()
    uTTC.enable_load_config_DIO5()
    uTTC.channel_DIO5(13)
    #uTTC.threshold_channel1_DIO5(27)
    uTTC.status_DIO5()
    uTTC.disable_load_config_DIO5()
    #uTTC.disable_DIO5()

    """
    eliminate time violation in the compilation
    bugs in the phase measurement (2.5 ns accuracy)
    - measure the phase
    - trigger rules for the LHC
    assyncronous trigger from the DIO5
    - loop back from the output of the DIO5 and feedback the loop generator
    - clear the address table -- to update on the c++

    - install the AMC13 tools

    -- update the repo - the binary to upload
     TTS feedback to work -- read back the TTS state on th microDTC

    - lock 2 from the assyncronous generator



    """

#---------------------------------------------------------------------------------
