/*!
  \file                  TTC_FC7Controller.h
  \brief                 Controller of the TTC_FC7, overall wrapper of the framework
  \author                Mauro DINARDO
  \version               2.0
  \date                  01/01/20
  Support:               email to mauro.dinardo@cern.ch
*/

#ifndef TTC_FC7_TTC_FC7CONTROLLER_H
#define TTC_FC7_TTC_FC7CONTROLLER_H

#include "../HWDescription/TTC_FC7.h"
#include "../HWInterface/TTC_FC7FWInterface.h"
#include "../HWInterface/TTC_FC7Interface.h"
#include "../Utils/FileHandler_TTC_FC7.h"
#include "../Utils/Event_TTC_FC7.h"
#include "../Utils/Utilities.h"
#include "FileParser_TTC_FC7.h" // EQUAL TO Ph2_ACF -- import from there?
#include <fstream>
#include <iostream>
#include <future>
// amc13 stuff
//#ifdef __AMC13__
//#include "../../amc13-uhal-2.8/dev_tools/sample_cpp_code/src/common/PixelAMC13Interface.h"
//#include "../../amc13-uhal-2.8/amc13/include/amc13/AMC13.hh"
//#endif
using namespace TTC_FC7_Utils; // Decoder, SettingsMap, tr_logger_sel_data
using namespace TTC_FC7_HwInterface;
//class DetectorMonitor;
/*!
 * \namespace TTC_FC7_System
 * \brief Namespace regrouping the framework wrapper
 */
namespace TTC_FC7_System
{

    /*!
 * \class TTC_FC7Controller
 * \brief Create, initialise, configure a predefined HW structure
 */
    class TTC_FC7Controller
    {
    public:
        TTC_FC7_HwInterface::TTC_FC7Interface *fTTC_FC7Interface;
        TTC_FC7_HwInterface::TTC_FC7FWInterface *fTTC_FC7FWInterface;
        TTC_FC7_HwDescription::TTC_FC7 *fTTC_FC7;
        TTC_FC7_Utils::SettingsMap fSettingsMap;
        // amc13 stuff
        //#ifdef __AMC13__
        //PixelAMC13Interface::PixelAMC13Interface *fPixelAMC13Interface;
        //#endif

        //DetectorMonitor*   fDetectorMonitor;
        bool               fWriteHandlerEnabled = false;
        FileHandler_TTC_FC7*       fFileHandler_TTC_FC7;
        std::string        fRawFileName;
        int                record_nOrbits;
        int                trigger_source;
        //int                trigger_interval:

        /*!
     * \brief Constructor of the TTC_FC7Controller class
     */
        TTC_FC7Controller();

        /*!
     * \brief Destructor of the TTC_FC7Controller class
     */
        virtual ~TTC_FC7Controller();

        /*!
         * \brief Method to construct a system controller object from another one while re-using the same members
         */
        void Inherit(const TTC_FC7Controller* pController);

        /*!
     * \brief Destroy the TTC_FC7Controller object: clear the HWDescription Objects, FWInterface etc.
     */
        void Destroy();

        /*!
     * \brief Initialize the Hardware via a config file
     * \param pFilename : HW Description file
     *\param os         : ostream to dump output
     */
        void InitializeHw(const std::string &pFilename, std::ostream &os = std::cout);

        void CreateResultDirectory(const std::string& pDirname, bool pDate = false); // bool pMode = true,
        //void InitResultFile(const std::string& pFilename);

        /*!
         * \brief Create a FileHandler_TTC_FC7 object with
         * \param pFilename : the filename of the binary file
         * EQUAL TO Ph2_ACF -- import from there?
         */
        void         addFileHandler_TTC_FC7(const std::string& pFilename, char pOption);
        void         closeFileHandler_TTC_FC7();
        void         WriteFileHandler_TTC_FC7(std::string log);
        //FileHandler_TTC_FC7* getFileHandler_TTC_FC7() { return fFileHandler_TTC_FC7; }

        /*!
         * \brief Issues a FileHandler_TTC_FC7 for writing files to every BeBoardFWInterface if addFileHandler_TTC_FC7 was called
         */
        void     initializeWriteFileHandler_TTC_FC7();
        //uint32_t computeEventSize32(const Ph2_HwDescription::BeBoard* pBoard);

        /*!
     * \brief Initialize the settings
     * \param pFilename : settings file
     *\param os         : ostream to dump output
     */
        void InitializeSettings(const std::string &pFilename, std::ostream &os = std::cout);

        /*!
     * \brief Configure the Hardware with XML file indicated values
     */
        void ConfigureHw();

        void Start(int runNumber);
        void Stop();
        void Pause();
        void Resume();
        //void Configure(std::string cHWFile);
        void Abort();
        void print_version();
        void Reset();
        void termination_and_test();
        void i2c_command();
        void enable_triggers();
        void disable_triggers();
        void configure();
        bool read_trigger_enable_state();
        void status_crt();
        void status_clk_gen();
        void DIO5_term_50_enable();
        std::map<std::string, int> count_lines(std::string file_name);
        void set_threshold();
        void send_trigger(int trigger_interval = 1200);
        void send_N_ipbus_triggers(int ntrigger);
        void status_cnt();
        void system_checks();
        void send_ipb_BX(int bx);
        void send_ipb_BCmd(int bcmd);
        void sync_orbit();
        int read_tts_state();
        bool test_tts_state();
        void read_tts_ideay();
        void scan_tts_idelay();
        void empty_FIFO();
        void reset_triggers();
        void embelezing(std::string message, auto value);
        void status(std::string message, bool state_bool);
        void ttc_log_dump_offline_decoding(int verbose_loc = -1, bool just_read = false);
        void Write_plain_log(std::string log, std::string after_str = "");
        int Write_ttc_log_dump(struct tr_logger_sel_data result, int verbose, FileHandler_TTC_FC7* &fFileHandler_TTC_FC7_internal);
        int ttc_log_dump_to_file(const struct tr_logger_sel_data entry, bool offline_decoding, bool log_in_binary, int verbose, FileHandler_TTC_FC7* &fFileHandler_TTC_FC7_internal); 
        int clean_FIFO(bool read_FIFO_size, int test_tr_logger_dcnt, std::vector<uint32_t> tr_logger_block_read, bool verbose);
        void Write_raw_log(uint32_t tr_logger_block_raw, bool err=false, int verbose=false);
        void Write_bin_log(uint32_t myData);
        std::vector<uint32_t> Read_bin_log(std::string file_to_read);
        std::string textBinaryVersion(uint32_t myData);
        uint32_t readValue(std::fstream &myFile, bool& goodRead);
        void initialize_AMC13();


        double findValueInSettings(const std::string name, double defaultValue = 0.) const;

        // related with data treatment
        uint32_t ReadData(std::vector<uint32_t>& pData, int& count_n_full, bool online_decoding, int verbose);
        void DecodeData(const std::vector<uint32_t>& pData, bool online_decoding);
        /*const std::vector<TTC_FC7_HwInterface::Event_TTC_FC7*>& GetEvents()
        {
         if(fFuture.valid() == true) fFuture.get();
         return fEventList;
        }*/
        const std::vector<struct tr_logger_sel_data>& GetEvents()
        {
         if(fFuture.valid() == true) fFuture.get();
         return fEventList;
        }

        std::string fDirectoryName; /*< the Directoryname for the Root file with results */

    private:
      FileParser_TTC_FC7 fParser;
      //std::vector<TTC_FC7_HwInterface::Event_TTC_FC7*> fEventList;
      std::vector<struct tr_logger_sel_data> fEventList;
      std::future<void>                    fFuture;
      Decoder decoder = Decoder();
      uint32_t saturation_point_FIFO = 131070;
      int tolerance_FIFO_repeat = 10;

    };
} // TTC_FC7_System

#endif
