/*!
  \file                  TTC_FC7Controller.cc
  \brief                 Controller of the System, overall wrapper of the framework
  \author                Alexandra CARVALHO
  \version               0.001 (in preparation)
  \date                  not ready
  Support:               email to alexandra.oliveira@cern.ch
*/

#include "TTC_FC7Controller.h"
#include "../Utils/FileHandler_TTC_FC7.h"
#include "../Utils/Timer.h"
#include "../Utils/Utilities.h" // currentDateTime
#include "../Utils/Dictionaries.h" // Description of xml options
#include <iostream>
#include <iomanip>
#include <future>
#include <tuple>
#include <unistd.h>
#include <string>
#include <map>
#include <bitset>

#include <boost/format.hpp>

// amc13 stuff
//#ifdef __AMC13__
//using namespace PixelAMC13Interface;
//#endif

using namespace std;
using namespace TTC_FC7_HwDescription;
using namespace TTC_FC7_HwInterface;
using namespace TTC_FC7_Utils; // Decoder, SettingsMap, tr_logger_sel_data

namespace TTC_FC7_System
{
    TTC_FC7Controller::TTC_FC7Controller()
        : fTTC_FC7Interface(nullptr)
        , fTTC_FC7FWInterface(nullptr)
        , fTTC_FC7(nullptr)
        , fFileHandler_TTC_FC7(nullptr)
        //, fWriteHandlerEnabled(false)
        , fRawFileName("")
        , trigger_source(-1)
        //, trigger_interval(-1)
        //, fDetectorMonitor(nullptr)
    {
    }

    TTC_FC7Controller::~TTC_FC7Controller()
    {
        Destroy();
    }

    void TTC_FC7Controller::Inherit(const TTC_FC7Controller* pController)
    {
    }

    void TTC_FC7Controller::Destroy()
    {
        delete fTTC_FC7Interface;
        fTTC_FC7Interface = nullptr;
        delete fTTC_FC7FWInterface;
        fTTC_FC7FWInterface = nullptr;
        delete fTTC_FC7;
        fTTC_FC7 = nullptr;
        //delete fPixelAMC13Interface;
        //fPixelAMC13Interface = nullptr;

        LOG(INFO) << BOLDRED << ">>> TTC_FC7Controller  destroyed <<<" << RESET;
    }

    void TTC_FC7Controller::InitializeHw(const std::string &pFilename, std::ostream &os)
    {
        this->fParser.parseHW(pFilename, fTTC_FC7FWInterface, fTTC_FC7, os);
        fTTC_FC7Interface = new TTC_FC7Interface(fTTC_FC7FWInterface, fTTC_FC7);
        std::vector<std::string> lstNames = fTTC_FC7Interface->getFpgaConfigList();

    }

    void TTC_FC7Controller::initialize_AMC13()
    {
      /*//--- inhibit most noise from uHAL
      uhal::setLogLevelTo(uhal::Error());
      // ../../amc13/etc/amc13/connections.xml -- the connection settings should be take from here
      std::string addressT1 = "/opt/cactus/etc/amc13/AMC13XG_T1.xml"; // this does not exist, but by now testing compilation
      std::string addressT2 = "/opt/cactus/etc/amc13/AMC13XG_T2.xml";

      //fPixelAMC13Interface = new PixelAMC13Interface(uriT1, uriT2);
      const std::string uriT1 = "chtcp-2.0://localhost:10203?target=192.168.4.165:50001";
      const std::string uriT2 = "chtcp-2.0://localhost:10203?target=192.168.4.164:50001";
      PixelAMC13Interface* pAMC13 = new PixelAMC13Interface(uriT1, addressT1, uriT2, addressT2);

      //PixelAMC13Interface::PixelAMC13Interface(
      //  std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&,
      //  std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&)
      //amc13::AMC13* amc13 = pAMC13->Get();
      //std::vector<std::string> lstNames = fTTC_FC7Interface->getFpgaConfigList();
      pAMC13->SetDebugPrints( true);

      pAMC13->SetNewWay( true);
      pAMC13->SetMask( "1");

      printf("configure...\n");
      //pAMC13->Configure();

      //amc13->startRun();
      */

    }

    void TTC_FC7Controller::InitializeSettings(const std::string &pFilename, std::ostream &os)
    {
        this->fParser.parseSettings(pFilename, fSettingsMap, os);
    }

    void TTC_FC7Controller::print_version()
    {
      LOG(INFO) << YELLOW << " Version: " << RESET;
      std::map<std::string, uint32_t> version_firmware;
      const std::vector<std::string> version_firmware_vec = {"usr_ver_major", "usr_ver_minor", "usr_ver_build", "usr_firmware_dd", "usr_firmware_mm", "usr_firmware_yy"};
      for (std::string const&  xx : version_firmware_vec)
      {
        version_firmware[xx] = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.usr_ver." + xx);
      }
      LOG(INFO) << YELLOW << " usr_ver_major/minor/build: " << version_firmware["usr_ver_major"] << "/" << version_firmware["usr_ver_minor"] << "/" << version_firmware["usr_ver_build"]  << RESET;
      LOG(INFO) << YELLOW << " Date: " << version_firmware["usr_firmware_dd"] << "/" << version_firmware["usr_firmware_mm"] << "/"<< version_firmware["usr_firmware_yy"] << RESET;
    }

    void TTC_FC7Controller::Reset()
    {
      LOG(INFO) << RED << " WARNING: reset not stable" << RESET;
      LOG(INFO) << GREEN << "      Resetting..." << RESET;
      LOG(INFO) << GREEN << "      - set_reset_all" << RESET;
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.reset_reg.global_rst", 1);
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.reset_reg.clk_gen_rst", 1);
      usleep(200000);
      LOG(INFO) << GREEN << "      - release_reset_all" << RESET;
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.reset_reg.global_rst", 0);
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.reset_reg.clk_gen_rst", 0);
      usleep(600000);
      LOG(INFO) << GREEN << "      Reset done" << RESET;
    }

    void TTC_FC7Controller::i2c_command()
    {

      LOG(INFO) << YELLOW << "i2c_command" << RESET;
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.i2c_command",0x00F40004 | 0x80000000);
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.i2c_command",0x00F40004);
      usleep(1000);

      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.i2c_command",0x00B80000 | 0x80000000);
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.i2c_command",0x00F40000);
      usleep(1000);

      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.i2c_command",0x00F40000 | 0x80000000);
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.i2c_command",0x00F40000);

    }

    void TTC_FC7Controller::enable_triggers()
    {
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.trigger.trigger_enable",1);
      usleep(100000);
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.trigger.trigger_enable",0);
      LOG(INFO) << YELLOW  << "Triggering started"  << RESET;
    }

    void TTC_FC7Controller::disable_triggers()
    {
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.trigger.trigger_disable",1);
      usleep(100000);
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.trigger.trigger_disable",0);
      LOG(INFO) << YELLOW  << "Triggering stopped"  << RESET;
    }

    uint32_t TTC_FC7Controller::ReadData(std::vector<uint32_t>& pData, int& count_n_full, bool online_decoding, int verbose)
    {
      uint32_t cNPackets = fTTC_FC7Interface->ReadData(false, pData, count_n_full);
      if(cNPackets == 0) 
      {
        // cont_nempty
        return cNPackets;
      }

      if ( cNPackets > saturation_point_FIFO - 1 )
      {
        LOG(INFO) << BOLDRED << "    WARNING: FIFO was saturated, number of consecutive times = " << count_n_full <<  RESET;
        count_n_full++;
      } else count_n_full = 0;


      if ( pData.size() != cNPackets )
      {
        LOG(INFO) << "    WARNING: pData.size() != cNPackets (" << pData.size() << ", " << cNPackets << ")" << "\n";
      }

      if( verbose == 1 )
      {
        LOG(INFO) << YELLOW << "Number of state transition log entries since last FIFO emptying: " << cNPackets << RESET;
      }
      
      this->DecodeData(pData, online_decoding); //get list of decpded data, the struct I did
      // std::vector<uint32_t> tr_logger_block_read
      /*if (!online_decoding)
      {
        fEventList.clear();
        //for(auto& evt: pData) fEventList.push_back(&evt); // else this is going to be on the list of decoded data
        for(auto evt: pData) fEventList.push_back(evt);
      }*/
      return cNPackets;
    }

    void TTC_FC7Controller::DecodeData(const std::vector<uint32_t>& pData, bool online_decoding) // make offline option to save to a decoded file after, to xchack
    {
      // ####################
      // # Decoding TTC data #
      // ####################
      fEventList.clear();

      if (pData.size() > 0)
      {
        //Timer time_to_decode;
        //time_to_decode.start();
        //int counter_orbit = 0;
        //bool verbose = 0;
        bool test_read_write = false;
        
        for (uint32_t const&  yy : pData)
        {
          if ( test_read_write ) std::cout << this->textBinaryVersion(yy) << std::endl;
          uint32_t yy_test = yy;
          struct tr_logger_sel_data result_log_dump = decoder.decode_log(yy_test, online_decoding);
          fEventList.push_back(result_log_dump);
          //counter_orbit += counter_orbit_local;
        }
        //LOG(INFO) << YELLOW << "There was " << counter_orbit <<"(" << pData.size() << ")" << " orbits (entries) in the run." << RESET;
        //this->closeFileHandler_TTC_FC7();
        //time_to_decode.stop();
        //LOG(INFO) << YELLOW << "Time to decode: " << time_to_decode.getElapsedTime() << " us" << RESET;
      }


    }


    void TTC_FC7Controller::status_crt()
    {
      usleep(300000);
      LOG(INFO) << GREEN << "Measuring clocks in the firmware ..." << RESET;
      LOG(INFO) << BLUE  << "------------------------------------------------" << RESET;
      for (auto const& xx : clocks_description)
      {
        int clk_rate  = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.clk_rate_" + std::to_string(xx.first));
        LOG(INFO) << GREEN << "clk_rate_" <<  xx.first << left << setw(2) << " --> " << left << setw(25) << xx.second  << right << setw(5) << clk_rate << RESET;
      }
      LOG(INFO) << BLUE  << "------------------------------------------------" << RESET;
      LOG(INFO) << BLUE  << "" << RESET;
    }

    void TTC_FC7Controller::set_threshold()
    {
      int set_trigger_threshold            = TTC_FC7Controller::findValueInSettings("set_trigger_threshold");
      int channel_to_set_threshold         = TTC_FC7Controller::findValueInSettings("channel_to_set_threshold");
      if (!(channel_to_set_threshold < 6 && channel_to_set_threshold > 0) && channel_to_set_threshold != -1)
      {
        channel_to_set_threshold = -1;
        LOG(INFO) << YELLOW  << "Invalid choice of 'channel_to_set_threshold' (" << channel_to_set_threshold <<")."  << RESET;
        LOG(INFO) << YELLOW  << "Should be between 1-5 or -1 (all). Setting it to -1."  << RESET;
      }
      //int test_thr = fTTC_FC7Interface->ReadBoardReg("user.ctrl_regs.ext_tlu_1.dio5_ch1_thr");
      //LOG(INFO) << YELLOW  << "  before set threshold in ch1 it is: "<<  test_thr << RESET;
      for (int ii = 1; ii < 6; ++ii) {
        if ( channel_to_set_threshold != ii && (channel_to_set_threshold < 6 && channel_to_set_threshold > 0) ) continue;
        fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.ext_dio5_tlu_1.dio5_ch" + std::to_string(ii) + "_thr", set_trigger_threshold);
        LOG(INFO) << YELLOW  << " Set trigger threshold of channel " << ii << " to " << set_trigger_threshold << RESET;
      }
      usleep(200000);
    }

    std::map<std::string, int> TTC_FC7Controller::count_lines(std::string file_name)
    {
      std::ifstream ff(file_name);
      std::string line;
      long ii;
      int iorbit = 0;
      for (ii = 0; std::getline(ff, line); ++ii)
      {
        if ( line.find( "ORB" ) != std::string::npos ) iorbit++;
      }
      return {
       {"nlines", ii},
       {"norbit", iorbit}
     };
    }

    bool TTC_FC7Controller::read_trigger_enable_state()
    {
      int trigger_source_read = fTTC_FC7Interface->ReadBoardReg("user.ctrl_regs.trigger.trigger_source");
      LOG(INFO) << GREEN << "The source is: " << trigger_source_read << RESET;

      int state  = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.trigger.trigger_enable_state"); // to update to user.stat_regs.trigger.trigger_enable_state
      std::string state_name; // = state == 1 ? "enabled" : "disabled";
      if ( state == 0 ) {state_name = "disabled";}
      else if ( state == 1 ) {state_name = "enabled";}
      else {state_name = "Undefined";}
      LOG(INFO) << YELLOW << "Trigger ouput is: " << state_name << " (" << state <<")" << RESET;
      return state;
    }

    void TTC_FC7Controller::status(std::string message, bool state_bool)
    {
      std::string state = state_bool ? "enabled" : "disabled" ;
      LOG(INFO) << YELLOW << message <<" " << state << RESET;
    }

    void TTC_FC7Controller::configure() {

      bool power_FMC_L8_L12 = TTC_FC7Controller::findValueInSettings("power_FMC_L8_L12");
      if ( power_FMC_L8_L12 ){
        fTTC_FC7Interface->WriteBoardReg("sysreg.reg_ctrl_2.fmc_l12_pwr_en", 1);
        fTTC_FC7Interface->WriteBoardReg("sysreg.reg_ctrl_2.fmc_l8_pwr_en", 1);
        fTTC_FC7Interface->WriteBoardReg("sysreg.reg_ctrl_2.fmc_pg_c2m", 1);
        LOG(INFO) << YELLOW << "L8 & L12 FMC power ON" << RESET;
      }

      bool i2c_enable = TTC_FC7Controller::findValueInSettings("i2c_enable");
      if ( i2c_enable )
      {
        fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.i2c_settings",0x0083e8);
        LOG(INFO) << YELLOW << "8SFP i2c master enabled" << RESET;
        TTC_FC7Controller::i2c_command();
      } else
      {
        fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.i2c_settings",0x000000);
        LOG(INFO) << YELLOW << "8SFP i2c master disabled" << RESET;
      }

      bool enable_DIO5 = TTC_FC7Controller::findValueInSettings("enable_DIO5");
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.ext_dio5_tlu_1.dio5_en",enable_DIO5);
      TTC_FC7Controller::status("DIO5", enable_DIO5);

      bool enable_dio5_load_config = TTC_FC7Controller::findValueInSettings("enable_dio5_load_config");
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.ext_dio5_tlu_2.dio5_load_config",enable_dio5_load_config);
      TTC_FC7Controller::status("DIO5 (load_config_DIO5);", enable_dio5_load_config);

      int channel_DIO5 = TTC_FC7Controller::findValueInSettings("channel_DIO5");
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.ext_dio5_tlu_1.dio5_ch_out_en",channel_DIO5);
      LOG(INFO) << YELLOW << "DIO5 output configured to: " << channel_DIO5 << RESET;

      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.ext_dio5_tlu_2.dio5_load_config",0);
      TTC_FC7Controller::status("DIO5 (load_config_DIO5):", 0);

      bool tts_fsm_disable = TTC_FC7Controller::findValueInSettings("tts_fsm_disable");
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.tts_idelay.tts_fsm_disable", tts_fsm_disable);
      TTC_FC7Controller::status("FSM reacting to TTS input from AMC13:", tts_fsm_disable);

      /*
      def set_trigger_rule(self, src):
          print ('Changing trigger rule to: ' + str(src))
          self.ipb_write("user.ctrl_regs.trigger.trigger_rule",src,1)
      // get on manual
      */

      // see the better order to put it
      bool i2c_reset = TTC_FC7Controller::findValueInSettings("i2c_reset");
      if ( i2c_reset ) {
        fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.reset_reg.i2c_rst", 1);
        LOG(INFO) << YELLOW  << "8SFP i2c master reset sent." << RESET;
      }

    LOG(INFO) << YELLOW << "Changing trigger source to: 0 (none) before seting up AMC13" << RESET;
    fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.trigger.trigger_source",0);
    TTC_FC7Controller::disable_triggers();

    LOG(INFO) << YELLOW << "Configuration complete!" << RESET;
    LOG(INFO) << YELLOW << "" << RESET;

    } // configure

    void TTC_FC7Controller::reset_triggers()
    {
      usleep(1000000);
      int set_external_trigger_select = TTC_FC7Controller::findValueInSettings("set_external_trigger_select");
      LOG(INFO) << YELLOW << "Set external trigger to " << set_external_trigger_select << " (" << set_external_trigger_select_description[set_external_trigger_select] << ")" << RESET;
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.trigger.external_trigger_select", set_external_trigger_select);

      LOG(INFO) << YELLOW << "Changing trigger source to: 0 (none) before configuring the AMC13" << RESET;
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.trigger.trigger_source", 0);
    }


    void TTC_FC7Controller::status_clk_gen()
    {
      int clk_40_locked_1 = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.status_clk_gen.clk_40_locked_1");
      int clk_40_locked_2 = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.status_clk_gen.clk_40_locked_2");
      int ref_clk_locked  = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.status_clk_gen.ref_clk_locked");
      LOG(INFO) << GREEN << "Reading clocks state..." << RESET;
      LOG(INFO) << BLUE  << "------------------------------------------------" << RESET;
      LOG(INFO) << GREEN << "clk_40_locked_1   " << clk_40_locked_1 << RESET;
      LOG(INFO) << GREEN << "clk_40_locked_2   " << clk_40_locked_2 << RESET;
      LOG(INFO) << GREEN << "ref_clk_locked    " << ref_clk_locked << RESET;
      LOG(INFO) << BLUE  << "------------------------------------------------" << RESET;
      LOG(INFO) << YELLOW << "" << RESET;

    }

    void TTC_FC7Controller::send_N_ipbus_triggers(int ntrigger)
    {
      LOG(INFO) << YELLOW << "Changing trigger source to 1 (ipbus) "  << RESET;
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.trigger.trigger_source", 1);

      TTC_FC7Controller::enable_triggers();

      LOG(INFO) << YELLOW << "Sending " << ntrigger << " single triggers via ipbus " << RESET;
      for (int ii = 0; ii < ntrigger; ii++ )
      {
          fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.trigger.ipb_trigger",1);
          LOG(INFO) << YELLOW << "    Trigger " << ii << " sent" << RESET;
          usleep(100000);
          fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.trigger.ipb_trigger",0);
          usleep(100000);
      }
    }

    void TTC_FC7Controller::status_cnt()
    {
        // uTTC.status_cnt()
        int evcnt  = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.counter_1.evcnt");
        int bcnt   = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.counter_1.bcnt");
        int orbcnt = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.counter_2.orbcnt");
        int evrcnt = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.counter_2.evrcnt");
        LOG(INFO) << YELLOW << "Counter values ..." << RESET;
        LOG(INFO) << YELLOW << "------------------------------------------------" << RESET;
        LOG(INFO) << YELLOW << "event counter             " << evcnt << RESET;
        LOG(INFO) << YELLOW << "bunch crossing counter    " << bcnt << RESET;
        LOG(INFO) << YELLOW << "orbit counter             " << orbcnt << RESET;
        LOG(INFO) << YELLOW << "event reject counter      " << evrcnt << RESET;
        LOG(INFO) << YELLOW << "------------------------------------------------" << RESET;

    }

    void TTC_FC7Controller::send_trigger(int trigger_rate)
    {
       trigger_source = TTC_FC7Controller::findValueInSettings("TriggerSource");

       std::string trigger_source_type = trigger_source_description[trigger_source]; // test first if the option exists
       LOG(INFO) << YELLOW << "Changing trigger source to: "<< trigger_source_type <<" ("<< trigger_source <<")" << RESET;
       fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.trigger.trigger_source",trigger_source);

       if (trigger_source == 7)
       {
         int trigger_interval = 40000./trigger_rate;
         LOG(INFO) << YELLOW << "Will set trigger frequency to: " << trigger_rate << "kHz (trigger interval = "<< trigger_interval << ")" << RESET;
         fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.clocked_trigger.trigger_interval",trigger_interval);
       }

       if (trigger_source > 0) TTC_FC7Controller::enable_triggers();
       TTC_FC7Controller::read_trigger_enable_state();

       if ( trigger_source == 5 )
       {
         // check if always when trigger source set to external also set external clocks
         fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.trigger.ext_clk_en",1);
         LOG(INFO) << YELLOW  << "Init external clock complete"  << RESET;
         usleep(200000);
         LOG(INFO) << YELLOW  << RESET;
       }

       if (trigger_source == 1)
       {
          // set default
          int ntrigger = TTC_FC7Controller::findValueInSettings("nTriggersIPBus");
          TTC_FC7Controller::send_N_ipbus_triggers(ntrigger);
       }
       LOG(INFO) << YELLOW << "Selection of triggering options complete!" << RESET;
       LOG(INFO) << YELLOW << "" << RESET;

    } // send_trigger    

    void TTC_FC7Controller::send_ipb_BX(int bx)
    {
        LOG(INFO) << YELLOW << "Sending BX: " << std::hex << bx << RESET;
        fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.ChB.ipb_BX",bx);
    }

    void TTC_FC7Controller::send_ipb_BCmd(int bcmd)
    {
        LOG(INFO) << YELLOW << "Sending BCmd: " << std::hex << bcmd << RESET;
        fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.ChB.ipb_BCmd",bcmd);
    }

    void TTC_FC7Controller::sync_orbit()
    {
      //[3]$ python3 ttc bc0 periodic_bc0_on
      //uTTC.bc0_enable()
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.ChB.bc0_enable",1);
      LOG(INFO) << YELLOW << "bc0 enabled" << RESET;
      usleep(100000);

      int zero = 0x000;
      int to_ec0 = 0x202;
      int to_bgo = 0x206;
      //[3]$ python3 ttc bgo ec0
      TTC_FC7Controller::send_ipb_BX(zero);
      TTC_FC7Controller::send_ipb_BCmd(to_ec0);
      TTC_FC7Controller::send_ipb_BX(zero);
      TTC_FC7Controller::send_ipb_BCmd(zero);

      //[3]$ python3 ttc bgo 
      TTC_FC7Controller::send_ipb_BX(zero);
      TTC_FC7Controller::send_ipb_BCmd(to_bgo);
      TTC_FC7Controller::send_ipb_BX(zero);
      TTC_FC7Controller::send_ipb_BCmd(zero);

    }

    void TTC_FC7Controller::embelezing(std::string message, auto value)
    {
      LOG(INFO) << BLUE  << "------------------------------------------------" << RESET;
      LOG(INFO) << GREEN  << message << " " <<  value << RESET;
      LOG(INFO) << BLUE  << "------------------------------------------------" << RESET;
      LOG(INFO) << YELLOW << "" << RESET;
    }

    int TTC_FC7Controller::read_tts_state()
    {
      int TTS_state = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.TTS_status.TTS_state");
      return TTS_state;
    }

    bool TTC_FC7Controller::test_tts_state()
    {
      // right now the test if the TTS state is stable shall be if the FIFO is not being filled
      int tr_logger_dcnt = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.tr_logger.dcnt");
      tr_logger_dcnt = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.tr_logger.dcnt");
      bool tts_stable = false;
      LOG(INFO) << BLUE  << "Size of FIFO " << tr_logger_dcnt << RESET;
      if (tr_logger_dcnt < 100) tts_stable = true;
      return tts_stable;
    }

    void TTC_FC7Controller::read_tts_ideay()
    {
      int outtap = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.tts_idelay.outtap");
      LOG(INFO) << YELLOW << "TTS decoder input phase delay ~ " << outtap*0.15 << " ns" << RESET;
    }

    void TTC_FC7Controller::empty_FIFO()
    {
      LOG(INFO) << GREEN << "Empty the FIFO 40 times without recording to arrive to a stable point." << RESET;
      for (int ii = 0; ii < 40; ii++ )
      {
        std::vector<uint32_t> tr_logger_block_read = fTTC_FC7Interface->ReadBlockBoardReg("user.stat_regs.tr_logger_dout", saturation_point_FIFO);
      }      
    }

    int TTC_FC7Controller::clean_FIFO(bool read_FIFO_size, int test_tr_logger_dcnt, std::vector<uint32_t> tr_logger_block_read, bool verbose)
    {
      int real_size_FIFO = 0;
      if ( !read_FIFO_size )
      {
        int count_repeated = 0;
        uint32_t tr_logger_block_raw_last;
        // loop to remove find the real FIFO size
        for (int ii = 0; ii < test_tr_logger_dcnt; ii++ )
        {
          uint32_t tr_logger_block_raw =  tr_logger_block_read[ii];
          if ( ii == 0 ) tr_logger_block_raw_last = tr_logger_block_raw;
          else
          {
            if (tr_logger_block_raw_last == tr_logger_block_raw) count_repeated++;
            else
            {
              count_repeated = 0;
              tr_logger_block_raw_last = tr_logger_block_raw;
            }
          }
          if ( count_repeated > tolerance_FIFO_repeat )
          {
            if ( verbose == 1 ) LOG(INFO) << YELLOW << "Will stoped recording FIFO in "<< real_size_FIFO << " as there was " << tolerance_FIFO_repeat << " consecutive entry repetitions. " << RESET;
            break;
          }
          real_size_FIFO++;
        }
      } else real_size_FIFO = tr_logger_block_read.size();
      return real_size_FIFO;
    }

    void TTC_FC7Controller::scan_tts_idelay()
    {
        // idelay = (0..31 in units of ~0.15 ns)
        int attempts = 0;
        bool TTS_state = TTC_FC7Controller::test_tts_state();
        while (!TTS_state) 
        {
          if (attempts > 31) 
          {
            LOG(ERROR) << BOLDRED << "Could not stabilize TTS state" << RESET;
            break;
          }

          LOG(INFO) << BLUE  << "Last TTS decoded state: " << TTS_state << ", scan attempt " << attempts << RESET;
          // set 
          fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.tts_idelay.intap",attempts);
          fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.tts_idelay.load",1);
          usleep(200000);
          fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.tts_idelay.load",0);
          TTS_state = TTC_FC7Controller::test_tts_state();
          attempts++;
        }
        if (TTS_state) LOG(INFO) << GREEN  << "TTS state ready!"  << RESET;
        // read
        TTC_FC7Controller::read_tts_ideay();

    }

    void TTC_FC7Controller::system_checks()
    {
      // start tests on registers
      bool status_crt = TTC_FC7Controller::findValueInSettings("status_crt");
      bool status_clk_gen = TTC_FC7Controller::findValueInSettings("status_clk_gen");
      bool read_tts_state = TTC_FC7Controller::findValueInSettings("read_tts_state");
      bool status_DIO5 = TTC_FC7Controller::findValueInSettings("status_DIO5");
      bool i2c_status = TTC_FC7Controller::findValueInSettings("status_DIO5");

      if ( status_crt ) TTC_FC7Controller::status_crt();
      if ( status_clk_gen ) TTC_FC7Controller::status_clk_gen();
      if ( read_tts_state )
      {
        int TTS_state = TTC_FC7Controller::read_tts_state();
        TTC_FC7Controller::embelezing("Last TTS decoded state: ", TTS_state);
      }
      if ( status_DIO5 )
      {
        //int imp_cnt  = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.imp_cnt");
        TTC_FC7Controller::embelezing("status_DIO5 (imp_cnt) = FIX THE REGISTER!", 0);
      }

      bool read_external_trigger_select = TTC_FC7Controller::findValueInSettings("read_external_trigger_select");
      if ( read_external_trigger_select )
      {
        uint32_t val = fTTC_FC7Interface->ReadBoardReg("user.ctrl_regs.trigger.external_trigger_select");
        TTC_FC7Controller::embelezing("External trigger is received from:", set_external_trigger_select_description[val]);
      }

      bool read_trigger_rule = TTC_FC7Controller::findValueInSettings("read_trigger_rule");
      if ( read_trigger_rule )
      {
        uint32_t val_rule = fTTC_FC7Interface->ReadBoardReg("user.ctrl_regs.trigger.trigger_rule");
        std::string rule = TTC_FC7Controller::textBinaryVersion(val_rule);
        TTC_FC7Controller::embelezing("Active trigger rules <IT><PS><2S><CMS>: ", rule.substr(rule.size() - 4));
      }


      if ( i2c_status )
      {
        uint32_t i2c_settings = fTTC_FC7Interface->ReadBoardReg("user.ctrl_regs.i2c_settings");
        uint32_t i2c_command  = fTTC_FC7Interface->ReadBoardReg("user.ctrl_regs.i2c_command");
        int i2c_reply         = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.i2c_reply");
        int reg_i2c_settings  = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.reg_i2c_settings");
        LOG(INFO) << BLUE  << "------------------------------------------------" << RESET;
        LOG(INFO) << BLUE  << "!!!!! check the i2c_reply / reg_i2c_settings value format to print !!!!" << RESET;
        LOG(INFO) << GREEN  << "i2c_settings "     <<  TTC_FC7Controller::textBinaryVersion(i2c_settings) << RESET; // hex(i2c_settings)
        LOG(INFO) << GREEN  << "i2c_command "      <<  TTC_FC7Controller::textBinaryVersion(i2c_command)  << RESET; // hex(i2c_command)
        LOG(INFO) << GREEN  << "i2c_reply "        <<  i2c_reply << RESET;
        LOG(INFO) << GREEN  << "reg_i2c_settings " <<  reg_i2c_settings << RESET;
        LOG(INFO) << BLUE  << "------------------------------------------------" << RESET;
        LOG(INFO) << YELLOW << "" << RESET;
        // check the i2c_reply / reg_i2c_settings value format to print
      }


      /*
      Check manual for source meaning
      # switching clock source to fmc_l8_clk1 external clock DIO5 channel 5
        def clock_source(self, source):
            self.ipb_write("system.reg_ctrl.clock_source",source,1)
            print("Clock source switched to:" + bin(source))
            */


    }

    void TTC_FC7Controller::ttc_log_dump_offline_decoding(int verbose_loc, bool just_read)
    {

      int max_entries = TTC_FC7Controller::findValueInSettings("max_entries"); // for tests
      int max_entries_by_file = TTC_FC7Controller::findValueInSettings("max_entries_by_file");
      bool read_FIFO_size = TTC_FC7Controller::findValueInSettings("read_FIFO_size");
      bool offline_decoding = TTC_FC7Controller::findValueInSettings("offline_decoding");
      if ( !read_FIFO_size ) LOG(INFO) << BOLDRED << "Warning: It will not read the FIFO size on the FW before draining it, it will try to find the size by a pattern. That is under investigation/debugging. Not default behaviour." << RESET;
      bool log_in_binary = TTC_FC7Controller::findValueInSettings("log_in_binary");
      if ( !log_in_binary ) LOG(INFO) << BOLDRED << "Warning: It will save the raw data in text file format, you will not be able to post-decode it." << RESET;
      bool test_read_write = TTC_FC7Controller::findValueInSettings("test_read_write");

      LOG(INFO) << YELLOW << "" << RESET;
      LOG(INFO) << YELLOW << "Testing if trigger is enabled before proceeding." << RESET;
      bool receiving_trigger = TTC_FC7Controller::read_trigger_enable_state();
      if (  !receiving_trigger && !just_read)
      {
        LOG(ERROR) << BOLDRED << "TTC_FC7Controller::ttc_log_dump(): Trigger not enabled, nothing to do." << RESET;
        return;
      }

      bool test_timming  = TTC_FC7Controller::findValueInSettings("test_timming_ttc_log_dump");
      bool test_timming_per_read = TTC_FC7Controller::findValueInSettings("test_timming_per_read");
      int delay_between_reads = TTC_FC7Controller::findValueInSettings("delay_between_reads");// microseconds

      Timer tt;
      Timer tt_by_read;
      Timer tt_only_read;
      Timer tt_by_loop;
      bool pDate = true;
      int verbose = verbose_loc > -1 ? verbose_loc : TTC_FC7Controller::findValueInSettings("verbose");
      std::string ttc_log_dump_dir = "ttc_log_dump";
      TTC_FC7Controller::CreateResultDirectory(ttc_log_dump_dir);
      bool keep_saving = true;
      int total_entries = 0;
      int total_entries_file = 0;
      bool stop_run = false;
      
      //Decoder decoder = Decoder(); // if !offline_decoding || decode_imediatelly

      if (!just_read)
      {
        if (trigger_source == 1) LOG(INFO) << YELLOW << "It will read the FIFO once, saving the raw result, and then disable the trigger " << RESET;
        else
        {
          if (max_entries != -1) LOG(INFO) << YELLOW << "It will record up to count " << max_entries << " entries, saving the raw result, and then disable the trigger." << RESET;
          LOG(INFO) << YELLOW << "It will save a maximum of " << max_entries_by_file << " entries by file." << RESET;
        }
      }

      std::string data_saving_format =  !offline_decoding ? ".decoded" : (log_in_binary ? ".bin" : ".raw") ; //
      std::vector<tr_logger> ttc_log_dump_file_logs;
      std::vector<int> nentries;
      std::vector<float> time_between_reads;
      std::vector<float> time_per_read;
      std::vector<float> time_per_loop;
      bool reached_total_max = true;
      int count_n_full = 0;
      tt_by_read.start();

      if ( !(trigger_source == 1) && !just_read ) TTC_FC7Controller::empty_FIFO();

      if (!just_read) LOG(INFO) << YELLOW << "Starting the calibration test." << RESET;
      int counter_orbit_run = 0;
      while ( reached_total_max && keep_saving )
      {
        int test_tr_logger_dcnt_by_file = 0;
        int count_n_empty = 0;

        std::string ttc_log_dump_file = std::string(ttc_log_dump_dir) + "/ttc_log_dump";
        if(pDate) ttc_log_dump_file += currentDateTime();

        tr_logger ttc_log_dump_file_log;
        ttc_log_dump_file_log.log_name = ttc_log_dump_file;
        ttc_log_dump_file += data_saving_format;
        this->addFileHandler_TTC_FC7(ttc_log_dump_file , 'w');
        this->initializeWriteFileHandler_TTC_FC7();
        if ( fFileHandler_TTC_FC7 == nullptr) 
        {
          LOG(ERROR) << BOLDRED << __PRETTY_FUNCTION__ << " File for log dump not opened." << RESET;
        }

        if ( test_timming ) tt.start();
        bool reached_file_max = false;
        while ( !reached_file_max && !stop_run && keep_saving)
        {
          if ( test_timming_per_read )
          {
            tt_by_read.stop();
            time_between_reads.push_back(tt_by_read.getElapsedTime());
            tt_by_read.start();
          }
          if ( test_timming_per_read ) tt_only_read.start();
          std::vector<uint32_t> cRawData(0);// = fTTC_FC7Interface->ReadBlockBoardReg("user.stat_regs.tr_logger_dout", tr_logger_dcnt);
          uint32_t test_tr_logger_dcnt = this->ReadData(cRawData, count_n_full, !offline_decoding, verbose); // count empty and full
          //fFileHandler_TTC_FC7->setData(cRawData);
          
          if ( count_n_full > 10 )
          {
            stop_run = true;
            LOG(INFO) << BOLDRED << "There was 10 consecutive reading attempts with saturated FIFO. Returning." << RESET;
          }

          if ( stop_run ) break;
      
          if ( test_timming_per_read ) tt_only_read.stop();
          time_per_read.push_back(tt_only_read.getElapsedTime());

          if (test_tr_logger_dcnt > 0)
          {
            std::vector<struct tr_logger_sel_data> cPh2NewEvents = this->GetEvents();
            count_n_empty = 0;

            tt_by_loop.start();
            int real_size_FIFO = clean_FIFO(read_FIFO_size, test_tr_logger_dcnt, cRawData, verbose);
            // In case we do not want to read the FIFO size (to save time) it was attempt to read always the saturated point, and remove the additional entries by putting a tolerance in repeated entries
            tt_by_loop.stop();
            time_per_loop.push_back(tt_by_loop.getElapsedTime());

            nentries.push_back(real_size_FIFO);
            total_entries += real_size_FIFO;
            total_entries_file += real_size_FIFO;
            test_tr_logger_dcnt_by_file += real_size_FIFO;

            // put the loop inside the writting
            for (struct tr_logger_sel_data cPh2Event : cPh2NewEvents)
            { 
              if ( stop_run ) break;
              struct tr_logger_sel_data cPh2Event_loc = cPh2Event;
              int counter_orbit_local = this->ttc_log_dump_to_file(cPh2Event_loc, offline_decoding, log_in_binary, verbose, fFileHandler_TTC_FC7); 
              counter_orbit_run += counter_orbit_local;
            }

          } else
          {
            if ( verbose == 1 ) LOG(INFO) << BOLDRED << "There was nothing read on FIFO " << RESET;
            count_n_empty++;
          }

          if ( delay_between_reads > 0 ) usleep(delay_between_reads);

          if ( count_n_empty > 100 )
          {
            stop_run = true;
            LOG(INFO) << BOLDRED << "There was 100 consecutive reading attempts with empty FIFO. Returning." << RESET;
          }

          if (trigger_source == 1)
          {
            keep_saving = false;
          }
          else {
            reached_file_max = (total_entries_file > max_entries_by_file);
            if ( reached_file_max ) total_entries_file = 0;
          }

        } // entries < max_entries_by_file
        this->closeFileHandler_TTC_FC7();
        tt.stop();

        if ( trigger_source == 1 || stop_run || (total_entries > max_entries && max_entries != -1) )
        {
          LOG(INFO) << GREEN << "Empty the FIFO before finish test." << RESET;
          for (int ii = 0; ii < 40; ii++ ) std::vector<uint32_t> tr_logger_block_read = fTTC_FC7Interface->ReadBlockBoardReg("user.stat_regs.tr_logger_dout", saturation_point_FIFO);

          TTC_FC7Controller::disable_triggers();
          TTC_FC7Controller::read_trigger_enable_state();
          keep_saving = false;
          LOG(INFO) << YELLOW << "Disabled triggers " << RESET;
        }

        ttc_log_dump_file_log.elapsed_time = tt.getElapsedTime();
        ttc_log_dump_file_log.test_tr_logger_dcnt_by_file = test_tr_logger_dcnt_by_file;
        ttc_log_dump_file_log.counter_orbit_by_file = 0; // TOFIX = remove from deffinition
        ttc_log_dump_file_logs.push_back(ttc_log_dump_file_log);

        if (trigger_source == 1) reached_total_max = true;
        else reached_total_max = (total_entries < max_entries) && max_entries != -1;

        this->closeFileHandler_TTC_FC7();
        LOG(INFO) << GREEN << "Result saved on file " << ttc_log_dump_file  << RESET;
      } // while total_entries < max_entries
      
      if (!offline_decoding) 
      {
        LOG(INFO) << GREEN << "There was " << counter_orbit_run << " orbits in the run." << RESET;
      }
      

      if ( test_timming_per_read ) tt_by_read.stop();
      time_between_reads.push_back(tt_by_read.getElapsedTime());

      if ( test_timming )
      {

        for (tr_logger const&  xx : ttc_log_dump_file_logs)
        {
          std::string ttc_log_dump_file_log = xx.log_name + ".log";
          std::string ttc_log_dump_file_raw = xx.log_name + data_saving_format;
          std::string ttc_log_dump_file_decoded = xx.log_name + ".decoded";
          int counter_orbit_by_file = 0;

          if ( log_in_binary && offline_decoding )
          {
            LOG(INFO) << YELLOW << "" << RESET;
            LOG(INFO) << YELLOW << "Will read the above binary file and decode the content." << RESET;

            this->addFileHandler_TTC_FC7(ttc_log_dump_file_raw , 'r');
            std::vector<uint32_t> result = this->Read_bin_log(ttc_log_dump_file_raw);
            this->closeFileHandler_TTC_FC7();

            this->addFileHandler_TTC_FC7(ttc_log_dump_file_decoded , 'w');
            this->initializeWriteFileHandler_TTC_FC7();
            for (uint32_t const&  yy : result)
            {
              uint32_t yy_test = yy;
              struct tr_logger_sel_data result_log_dump = decoder.decode_log(yy_test, true);
              if ( result_log_dump.type_log != "Err" )
              {
                int counter_orbit_local = this->Write_ttc_log_dump(result_log_dump, verbose, fFileHandler_TTC_FC7);
                counter_orbit_by_file += counter_orbit_local;
              } else
              {
                this->Write_raw_log(yy, true, verbose);
                LOG(INFO) << BOLDRED << "Could not decode the entry. " << TTC_FC7Controller::textBinaryVersion(yy_test) << RESET;
              }
              if ( test_read_write ) std::cout << TTC_FC7Controller::textBinaryVersion(yy) << std::endl;
            }
            LOG(INFO) << YELLOW << "There was " << counter_orbit_by_file << " orbits in the file." << RESET;
            this->closeFileHandler_TTC_FC7();

          }

          int trigger_interval = fTTC_FC7Interface->ReadBoardReg("user.ctrl_regs.clocked_trigger.trigger_interval");
          int trigger_rate = 40000./trigger_interval;

          std::ofstream myfiletimming;
          myfiletimming.open(ttc_log_dump_file_log, ios::out);
          myfiletimming << "elapsed_time (s) :         " << xx.elapsed_time << "\n";
          myfiletimming << "entries produced :     " << xx.test_tr_logger_dcnt_by_file << "\n";
          if ( !log_in_binary )
          {
            std::map<std::string, int> num_lines = TTC_FC7Controller::count_lines(ttc_log_dump_file_raw);
            LOG(INFO) << YELLOW << "" << RESET;
            LOG(INFO) << YELLOW << "Output file  " << ttc_log_dump_file_log << "  has " << num_lines["nlines"] << " lines " << RESET;
            myfiletimming << "entries in log :       " << num_lines["nlines"] << "\n";
          }
          myfiletimming << "trigger rate (kHz) :     " << trigger_rate << "\n";
          myfiletimming << "nentries :     ";
          for (int const&  yy : nentries) myfiletimming << yy <<"  ";
          myfiletimming << "\n";
          if ( test_timming_per_read )
          {
            myfiletimming << "elapsed time_between_reads(us) :     ";
            for (float const&  yy : time_between_reads) myfiletimming << yy*1e+6 <<"  ";
            myfiletimming << "\n";

            myfiletimming << "elapsed time_per_read(us) :     ";
            for (float const&  yy : time_per_read) myfiletimming << yy*1e+6 <<"  ";
            myfiletimming << "\n";

            if ( !read_FIFO_size )
            {
            myfiletimming << "elapsed time_per_loop to remove duplicates(us) :     ";
            for (float const&  yy : time_per_loop) myfiletimming << yy*1e+6 <<"  ";
            myfiletimming << "\n";
            }

          }
          myfiletimming << "no of FIFO drains :     " << nentries.size() << "\n";
          if ( counter_orbit_by_file > 0 ) myfiletimming << "no of orbits read:     " << counter_orbit_by_file << "\n";
          myfiletimming.close();
          LOG(INFO) << GREEN << "Saving binary data into: " << RESET << BOLDYELLOW << ttc_log_dump_file_log << RESET;
          }
      }

    } // ttc_log_dump_offline_decoding

    void TTC_FC7Controller::DIO5_term_50_enable()
    {
      int channel = TTC_FC7Controller::findValueInSettings("channel_to_set_internal_termination");

      if (channel_termination_description.find(channel) == channel_termination_description.end())
      {
        LOG(INFO) << YELLOW << "Options for 'channel_to_set_internal_termination' " << RESET;
        for(auto const& itr : channel_termination_description)
        {
          LOG(INFO) << YELLOW << "Number  " << itr.first << " for " << itr.second << RESET;
        }
        LOG(ERROR) << BOLDRED << "Invalid choice for 'channel_to_set_internal_termination', see valid options above" << RESET;
        return;
      }

      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.ext_dio5_tlu_1.dio5_en", 1);
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.ext_dio5_tlu_1.dio5_ch_out_en", 0);
      fTTC_FC7Interface->WriteBoardReg("user.ctrl_regs.ext_dio5_tlu_1.dio5_term_50ohm_en", channel);
      LOG(INFO) << YELLOW << "     Set 50 ohm internal terminations to " << channel_termination_description[channel]  << RESET;
      usleep(200000);

    }

    void TTC_FC7Controller::termination_and_test()
    {
      LOG(INFO) << BOLDBLUE << "Sequence of commands to add termination and trigger counting. " << int(fTTC_FC7->getId()) << RESET;
      TTC_FC7Controller::DIO5_term_50_enable();
      TTC_FC7Controller::set_threshold();
      TTC_FC7Controller::send_trigger(); // should be extenal for this
      TTC_FC7Controller::status_clk_gen();
      //TTC_FC7Controller::ttc_log_dump_offline_decoding();



      //LOG(INFO) << GREEN << "Check number of trigger hits 30 times" << RESET;
      //for (int ii = 0; ii < 30; ii++ )
      //{
      //  int trigger_hits = fTTC_FC7Interface->ReadBoardReg("user.stat_regs.trigger_cntr");
      //  LOG(INFO) << GREEN << "  -- number of triggers: " << trigger_hits << RESET;
      //}

    }

    void TTC_FC7Controller::addFileHandler_TTC_FC7(const std::string& pFilename, char pOption)
    {
        if(pOption == 'r')
            fFileHandler_TTC_FC7 = new FileHandler_TTC_FC7(pFilename, pOption);
        else if(pOption == 'w')
        {
            fRawFileName         = pFilename;
            fWriteHandlerEnabled = true;
        }
    }

    void TTC_FC7Controller::closeFileHandler_TTC_FC7()
    {
        if(fFileHandler_TTC_FC7 != nullptr)
        {
            if(fFileHandler_TTC_FC7->isFileOpen() == true) fFileHandler_TTC_FC7->closeFile();
            delete fFileHandler_TTC_FC7;
            fFileHandler_TTC_FC7 = nullptr;
        }
    }

    void TTC_FC7Controller::ConfigureHw()
    {

      if(fTTC_FC7 == nullptr)
      {
          LOG(ERROR) << BOLDRED << "Hardware not initialized: run TTC_FC7Controller::InitializeHw first" << RESET;
          return;
      }
      LOG(INFO) << BOLDMAGENTA << "@@@ Configuring HW parsed from xml file @@@" << RESET;
      // ######################################
      // # Configuring Inner Tracker hardware #
      // ######################################
      LOG(INFO) << BOLDBLUE << "\t--> Found an TTC FC7 board" << RESET;
      fTTC_FC7Interface->ConfigureBoard(fTTC_FC7);
      LOG(INFO) << GREEN << "Successfully configured Board " << int(fTTC_FC7->getId()) << RESET;
      LOG(INFO) << BOLDBLUE << "Now going to configure chips on Board " << int(fTTC_FC7->getId()) << RESET;
      //fTTC_FC7Interface->RebootBoard();

      //if(fDetectorMonitor != nullptr)
      //{
      //    LOG(INFO) << GREEN << "Starting monitoring thread" << RESET;
      //    fDetectorMonitor->startMonitoring();
      //}
    }

    void TTC_FC7Controller::Start(int runNumber)
    {
        //for(auto cBoard: *fDetectorContainer) fTTC_FC7Interface->Start(cBoard);
    }

    void TTC_FC7Controller::Stop()
    {
        fTTC_FC7Interface->Stop();
    }

    void TTC_FC7Controller::Pause()
    {
        fTTC_FC7Interface->Pause();
    }

    void TTC_FC7Controller::Resume()
    {
        fTTC_FC7Interface->Resume();
    }

    void TTC_FC7Controller::Abort()
    {
        LOG(ERROR) << BOLDRED << __PRETTY_FUNCTION__ << " Abort not implemented" << RESET;
    }

    double TTC_FC7Controller::findValueInSettings(const std::string name, double defaultValue) const
    {
        auto setting = fSettingsMap.find(name);
        return (setting != std::end(fSettingsMap) ? setting->second : defaultValue);
    }

    void TTC_FC7Controller::CreateResultDirectory(const std::string& pDirname, bool pDate) // bool pMode,
    {
        std::string fDirectoryName = "";
        // Fabio: CBC specific -> to be moved out from Tool_simple - BEGIN
        std::string nDirname = pDirname;
        if(pDate) nDirname += currentDateTime();
        LOG(INFO) << GREEN << "Creating directory: " << BOLDYELLOW << nDirname << RESET;
        std::string cCommand = "mkdir -p " + nDirname;
        try
        {
            system(cCommand.c_str());
        }
        catch(std::exception& e)
        {
            LOG(ERROR) << "Excepting when trying to create Result Directory: " << e.what();
        }

        fDirectoryName = nDirname;
    }

    void TTC_FC7Controller::initializeWriteFileHandler_TTC_FC7()
    {
        std::string cFilename = fRawFileName;
        fFileHandler_TTC_FC7 = new FileHandler_TTC_FC7(cFilename, 'w');
        //fBeBoardInterface->SetFileHandler_TTC_FC7(cBoard, fFileHandler_TTC_FC7); // link it to the board, that here is only one
        LOG(INFO) << GREEN << "Saving binary data into: " << RESET << BOLDYELLOW << cFilename << RESET;
    }

    void TTC_FC7Controller::Write_plain_log(std::string log, std::string after_str)
    {
      fFileHandler_TTC_FC7->appendFile(log, after_str);
    }

    void TTC_FC7Controller::Write_raw_log(uint32_t tr_logger_block_raw, bool err, int verbose)
    {
      std::bitset<32> tr_logger_block_raw_bin = std::bitset<8*sizeof(tr_logger_block_raw)>(tr_logger_block_raw);
      if (err) fFileHandler_TTC_FC7->appendFile("ERR! raw : ", " ");
      fFileHandler_TTC_FC7->appendRawToFile(tr_logger_block_raw_bin);
    }

    void TTC_FC7Controller::Write_bin_log(uint32_t tr_logger_block_raw) {
      fFileHandler_TTC_FC7->appendToBinFile(tr_logger_block_raw);
    }

    std::vector<uint32_t> TTC_FC7Controller::Read_bin_log(std::string file_to_read) {
      std::vector<uint32_t> result = fFileHandler_TTC_FC7->readFile_bin();
      return result;
    }

    std::string TTC_FC7Controller::textBinaryVersion(uint32_t myData) {
      std::string result = "";

      for (int i=0; i<32; ++i) {
        if ((myData>>i)&0x1) result = "1" + result;
        else result = "0" + result;
      }
      return result;
    }

    uint32_t TTC_FC7Controller::readValue(std::fstream &myFile, bool& goodRead) {
      uint32_t myData = 0;
      uint8_t aChar3;
      uint8_t aChar2;
      uint8_t aChar1;
      uint8_t aChar0;
      myFile >> aChar3;
      myFile >> aChar2;
      myFile >> aChar1;
      myFile >> aChar0;
      myData = (aChar3<<3*8) | (aChar2<<2*8) | (aChar1<<1*8) | aChar0;

      if (myFile.eof()) {
        goodRead=false;
        return 0;
      }
      return myData;
    }

    int TTC_FC7Controller::Write_ttc_log_dump(struct tr_logger_sel_data result, int verbose, FileHandler_TTC_FC7* &fFileHandler_TTC_FC7_internal)
    {
      int counter_orbit_local = 0;
      if ( verbose == 2 ) 
      {
        LOG(INFO) << YELLOW << result.type_log << ", " << result.type_result1 << "=" << result.result1 << ", "<< result.type_result2 << "=" << result.log_result << RESET; 
      }
      if ( fFileHandler_TTC_FC7_internal != nullptr)
      {
        fFileHandler_TTC_FC7_internal->appendFile(result.type_log, ", ");
        fFileHandler_TTC_FC7_internal->appendFile(result.type_result1, "=");
        fFileHandler_TTC_FC7_internal->appendFile(std::to_string(result.result1), ", ");
        fFileHandler_TTC_FC7_internal->appendFile(result.type_result2, "=");
        fFileHandler_TTC_FC7_internal->appendFile(result.log_result, "\n");
      }
      if ( result.type_result1 == "ORB" ) counter_orbit_local++;
      return counter_orbit_local;
    }

    int TTC_FC7Controller::ttc_log_dump_to_file(const struct tr_logger_sel_data entry, bool offline_decoding, bool log_in_binary, int verbose, FileHandler_TTC_FC7* &fFileHandler_TTC_FC7_internal) 
    {

      int counter_orbit_local = 0;
      //for (struct tr_logger_sel_data entry : decoded_FIFO)
      //{
        
        if (!offline_decoding)
        {
          fFileHandler_TTC_FC7_internal->appendFile(entry.type_log, ", ");
          fFileHandler_TTC_FC7_internal->appendFile(entry.type_result1, "=");
          fFileHandler_TTC_FC7_internal->appendFile(std::to_string(entry.result1), ", ");
          fFileHandler_TTC_FC7_internal->appendFile(entry.type_result2, "=");
          fFileHandler_TTC_FC7_internal->appendFile(entry.log_result, "\n");

          if ( verbose == 2 ) 
          {
            LOG(INFO) << YELLOW << entry.type_log << ", " << entry.type_result1 << "=" << entry.result1 << ", "<< entry.type_result2 << "=" << entry.log_result << RESET; 
          }
        } else if ( !log_in_binary)
        {
          uint32_t tr_logger_block_raw = entry.raw_data;
          std::bitset<32> tr_logger_block_raw_bin = std::bitset<8*sizeof(tr_logger_block_raw)>(tr_logger_block_raw);
          if (entry.type_log == "Err") fFileHandler_TTC_FC7_internal->appendFile("ERR! raw : ", " ");
          fFileHandler_TTC_FC7_internal->appendRawToFile(tr_logger_block_raw_bin);
        } else fFileHandler_TTC_FC7_internal->appendToBinFile(entry.raw_data);
        
        if ( entry.type_result1 == "ORB" ) counter_orbit_local++;
      //}
      return counter_orbit_local;

    }


    //#ifdef __USE_ROOT__
    /*void TTC_FC7Controller::InitResultFile(const std::string& pFilename)
    {
        if(!fDirectoryName.empty())
        {
            std::string cFilename = fDirectoryName + "/" + pFilename + ".root";

            try
            {
                fResultFile     = TFile::Open(cFilename.c_str(), "RECREATE");
                fResultFileName = cFilename;
            }
            catch(std::exception& e)
            {
                LOG(ERROR) << "Excepting when trying to create Result File: " << e.what();
            }
        }
        else
            LOG(INFO) << RED << "ERROR: " << RESET << "No Result Directory initialized - not saving results!";
    }*/



} // namespace TTC_FC7_System
