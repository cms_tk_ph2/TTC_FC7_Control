/*!
  \file                  FileHandler_TTC_FC7.cc
  \brief                 Binary file handler
  \author                Mauro DINARDO
  \version               1.0
  \date                  28/06/18
  Support:               email to mauro.dinard@cern.ch
*/

#include "FileHandler_TTC_FC7.h"
#include <cstdio>

//FileHandler_TTC_FC7::FileHandler_TTC_FC7(const std::string& pBinaryFileName, char pOption) : fHeader(), fHeaderPresent(false), fOption(pOption), fBinaryFileName(pBinaryFileName), fFileIsOpened(false)
FileHandler_TTC_FC7::FileHandler_TTC_FC7(const std::string& pBinaryFileName, char pOption) : fHeaderPresent(false), fOption(pOption), fBinaryFileName(pBinaryFileName), fFileIsOpened(false)
{
    FileHandler_TTC_FC7::openFile();

    if(fOption == 'w') fThread = std::thread(&FileHandler_TTC_FC7::writeFile, this);
}

/*FileHandler_TTC_FC7::FileHandler_TTC_FC7(const std::string& pBinaryFileName, char pOption, FileHeader pHeader)
    : fHeader(pHeader), fHeaderPresent(true), fOption(pOption), fBinaryFileName(pBinaryFileName), fFileIsOpened(false)
{
    FileHandler_TTC_FC7::openFile();

    if(fOption == 'w') fThread = std::thread(&FileHandler_TTC_FC7::writeFile, this);
}*/

FileHandler_TTC_FC7::~FileHandler_TTC_FC7()
{
    while(fQueue.empty() == false) usleep(DESTROYSLEEP);
    FileHandler_TTC_FC7::closeFile();
}

/*bool FileHandler_TTC_FC7::getHeader(FileHeader& theHeader) const
{
    if(fHeaderPresent == true)
    {
        theHeader = fHeader;
        return true;
    }

    FileHeader tmpHeader;
    theHeader = tmpHeader;
    return false;
}*/

void FileHandler_TTC_FC7::setData(std::vector<uint32_t>& pVector)
{
    std::lock_guard<std::mutex> cLock(fMutex);
    fQueue.push(pVector);
}

bool FileHandler_TTC_FC7::isFileOpen()
{
    std::lock_guard<std::mutex> cLock(fMemberMutex);
    return fFileIsOpened;
}

void FileHandler_TTC_FC7::rewind()
{
    std::lock_guard<std::mutex> cLock(fMemberMutex);

    if((fOption == 'r') && (FileHandler_TTC_FC7::isFileOpen() == true))
    {
        //if(fHeader.fValid == true)
        //    fBinaryFile.seekg(48, std::ios::beg);
        //else
            fBinaryFile.seekg(0, std::ios::beg);
    }
    //else
    //    LOG(ERROR) << BOLDRED << "You should not try to rewind a file opened in write mode (or file not open)" << RESET;
}

bool FileHandler_TTC_FC7::openFile()
{
    if(FileHandler_TTC_FC7::isFileOpen() == false)
    {
        std::lock_guard<std::mutex> cLock(fMemberMutex);

        if(fOption == 'w')
        {
            fBinaryFile.open((getFilename()).c_str(), std::fstream::trunc | std::fstream::out | std::fstream::binary);

            //if(fHeader.fValid == false)
            //{
                //LOG(WARNING) << GREEN << "Invalid file Header provided, writing file without it ..." << RESET;
                fHeaderPresent = false;
            /*}
            else if(fHeader.fValid)
            {
                std::vector<uint32_t> cHeaderVec = fHeader.encodeHeader();
                fBinaryFile.write((char*)&cHeaderVec.at(0), cHeaderVec.size() * sizeof(uint32_t));
                fHeaderPresent = true;
            }*/
        }
        else if(fOption == 'r')
        {
            fBinaryFile.open(getFilename().c_str(), std::fstream::in | std::fstream::binary);
            //fHeader.decodeHeader(FileHandler_TTC_FC7::readFileChunks(fHeader.fHeaderSize));

            //if(fHeader.fValid == false)
            //{
                fHeaderPresent = false;
                //LOG(WARNING) << BOLDRED << "No valid header found in binary file: " << BOLDYELLOW << fBinaryFileName << BOLDRED << " - resetting to begin of file and treating as normal data" << RESET;
                fBinaryFile.clear();
                fBinaryFile.seekg(0, std::ios::beg);
            /*}
            else if(fHeader.fValid == true)
            {
                LOG(INFO) << GREEN << "Found a valid header in binary file: " << BOLDYELLOW << fBinaryFileName << RESET;
                fHeaderPresent = true;
            }*/
        }

        fFileIsOpened = true;
    }

    return fFileIsOpened;
}

void FileHandler_TTC_FC7::closeFile()
{
    if(fFileIsOpened == true)
    {
        fFileIsOpened = false;
        if((fOption == 'w') && (fThread.joinable() == true)) fThread.join();
        fBinaryFile.close();
        //LOG(INFO) << GREEN << "Closed binary file: " << BOLDYELLOW << fBinaryFileName << RESET;
    }
}

std::vector<uint32_t> FileHandler_TTC_FC7::readFile()
{
    std::vector<uint32_t> cVector;
    uint32_t              word;

    fBinaryFile.seekg(0, std::ios::end);
    double fileSize = fBinaryFile.tellg();
    cVector.reserve(fileSize * sizeof(char) / sizeof(uint32_t));
    fBinaryFile.seekg(0, std::ios::beg);

    while(true)
    {
        fBinaryFile.read((char*)&word, sizeof(uint32_t));
        if(fBinaryFile.eof() == true) break;
        cVector.emplace_back(word);
    }

    closeFile();
    return cVector;
}


std::vector<uint32_t> FileHandler_TTC_FC7::readFile_bin()
{
    std::vector<uint32_t> cVector;
    uint32_t              word;

    fBinaryFile.seekg(0, std::ios::end);
    double fileSize = fBinaryFile.tellg();
    cVector.reserve(fileSize * sizeof(char) / sizeof(uint32_t));
    fBinaryFile.seekg(0, std::ios::beg);

    while(true)
    {
        //char aChar3;
        //char aChar2;
        //char aChar1;
        //char aChar0;

        fBinaryFile.read((char*)&word, sizeof(uint32_t));
        if(fBinaryFile.eof() == true) break;
        //word = (aChar3<<3*4) | (aChar2<<2*4) | (aChar1<<1*4) | aChar0;
        cVector.emplace_back(word);

        /*
        //fprintf(fBinaryFile, "%c", aChar);
        fBinaryFile << aChar;
        */


        /*uint32_t myData = 0;
        char aChar3;
        char aChar2;
        char aChar1;
        char aChar0;
        fscanf(myFile, "%c%c%c%c", &aChar3, &aChar2, &aChar1, &aChar0);
        if (feof(myFile)) {
          goodRead=false;
          return 0;
        }
        myData = (aChar3<<3*4) | (aChar2<<2*4) | (aChar1<<1*4) | aChar0;
        goodRead=true;
        return myData;
        //*/
    }

    closeFile();
    return cVector;
}

std::vector<uint32_t> FileHandler_TTC_FC7::readFileChunks(uint32_t pNWords)
{
    std::vector<uint32_t> cVector;
    uint32_t              word;

    for(auto i = 0u; i < pNWords; i++)
    {
        fBinaryFile.read((char*)&word, sizeof(uint32_t));

        if(fBinaryFile.eof() == true)
        {
            //LOG(WARNING) << BOLDRED << "Attention, input file " << BOLDYELLOW << fBinaryFileName << BOLDRED << " ended before reading " << BOLDYELLOW << pNWords << " 32-bit words" << RESET;
            closeFile();
            break;
        }

        cVector.emplace_back(word);
    }

    return cVector;
}

void FileHandler_TTC_FC7::writeFile()
{
    while(fFileIsOpened == true)
    {
        std::vector<uint32_t> cData;
        bool                  cDataPresent = FileHandler_TTC_FC7::dequeue(cData);

        if((cDataPresent == true) && (cData.size() != 0))
        {
            std::lock_guard<std::mutex> cLock(fMemberMutex);
            fBinaryFile.write((char*)&cData.at(0), cData.size() * sizeof(uint32_t));
            fBinaryFile.flush();
        }
    }
}

void FileHandler_TTC_FC7::appendFile(std::string log, std::string after_str)
{
  if (fFileIsOpened == true)
  {
    fBinaryFile << log << after_str;
  }
}

void FileHandler_TTC_FC7::appendRawToFile(std::bitset<32> log)
{
  if (fFileIsOpened == true)
  {
    fBinaryFile << log << "\n";
  }
}

void FileHandler_TTC_FC7::appendToBinFile(uint32_t log)
{
  if (fFileIsOpened == true)
  {
    uint8_t aChar;
    for (int i=0; i<=3; i+=1)
    {
      aChar = (log>>i*8);
      fBinaryFile << aChar;
    }
  }
}

bool FileHandler_TTC_FC7::dequeue(std::vector<uint32_t>& pData)
{
    std::lock_guard<std::mutex> cLock(fMutex);

    if(fQueue.empty() == false)
    {
        pData = fQueue.front();
        fQueue.pop();
        return true;
    }

    return false;
}
