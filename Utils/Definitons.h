#ifndef TTC_FC7_Definitions
#define TTC_FC7_Definitions

#include <unordered_map>
#include <string>
#include <iostream>
#include <map>
#include <vector>

namespace TTC_FC7_Utils
{
  using SettingsMap = std::unordered_map<std::string, double>; /*!< Maps the settings */

  struct tr_logger_sel_data
  {
    std::string type_log;
    std::string type_result1;
    std::string type_result2;
    std::map<uint32_t, std::string> type_log_result;
    uint32_t result1;
    uint32_t result2;
    uint32_t raw_data;
    std::string log_result;
  };

  struct tr_logger
  {
    std::string log_name;
    double elapsed_time;
    int test_tr_logger_dcnt_by_file;
    int counter_orbit_by_file;
  };

  class Decoder
  {

    public:

      Decoder();
      ~Decoder();

      struct tr_logger_sel_data decode_log(uint32_t& tr_logger_block_raw, bool online_decoding);
      void hello_world();

      // --- Maps for log result
      const std::map<uint32_t, std::string> CHB_log_result = 
      {
        {0x1, "EC0"},
        {0x2, "BC0"},
        {0x3, "OC0"},
        {0x4, "Resync"},
        {0x5, "Reset"},
        {0x6, "Hard Reset"},
        {0x7, "start L1A"},
        {0x8, "stop L1A"},
        {0x9, "Cal. trigger"},
        {0xa, "EC0+BC0"},
        {0xb, "periodic EC0"},
        {0xc, "periodic BC0"},
        {0xd, "periodic EC0+BC0"},
        {0xe, "wait n. BX"},
        {0xf, "wait n. ORB"}
      };

      const std::map<uint32_t, std::string> TTS_log_result = 
      {
        {0x2, "OOS(2)"},
        {0x4, "BSY(4)"},
        {0x8, "RDY(8)"}
      };

      const std::map<uint32_t, std::string> BGO_log_result = 
      {
        {0x1, "EC0"},
        {0x2, "BC0"},
        {0x3, "OC0"},
        {0x4, "Resync"},
        {0x5, "Reset"},
        {0x10, "TCDS 0/Start"},
        {0x11, "TCDS 1/Stop"},
        {0x14, "TCDS 4/Resync"},
        {0x15, "TCDS 5/Reset"},
        {0x17, "TCDS 7/Sleep"},
        {0x41, "immediate ECO"},
        {0x42, "immediate BCO"},
        {0x43, "immediate OCO"},
        {0x44, "immediate Resync"},
        {0x45, "immediate Reset"}
      };



    //protected:
      //std::vector<uint32_t>& tr_logger_block_read_;

    //private:
    //  std::map<uint32_t, struct tr_logger_sel_data > tr_logger_map = {0x0, {"Err", "Err", "Err",    {0x0, "Err"}, 0x0, 0x0}};

  };
}

#endif
