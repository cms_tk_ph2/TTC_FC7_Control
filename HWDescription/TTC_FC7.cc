/*!

        Filename :                              TTC_FC7.cc
        Content :                               TTC_FC7 Description class, configs of the TTC_FC7
        Programmer :                    Lorenzo BIDEGAIN
        Version :               1.0
        Date of Creation :              14/07/14
        Support :                               mail to : lorenzo.bidegain@gmail.com

 */

#include "TTC_FC7.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

namespace TTC_FC7_HwDescription
{
// Constructors

TTC_FC7::TTC_FC7() : fBoardId(0) {}

TTC_FC7::TTC_FC7(uint8_t pBeId) : fBoardId(pBeId) {}


// Public Members:

uint32_t TTC_FC7::getReg(const std::string& pReg) const
{
    TTC_FC7RegMap::const_iterator i = fRegMap.find(pReg);

    if(i == fRegMap.end())
    {
        LOG(INFO) << "The Board object: " << +getId() << " doesn't have " << pReg;
        return 0;
    }
    else
        return i->second;
}

void TTC_FC7::setReg(const std::string& pReg, uint32_t psetValue) { fRegMap[pReg] = psetValue; }

} // namespace TTC_FC7_HwDescription
