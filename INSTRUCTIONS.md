# Instructions

Here are the instructions to use the test modules for which this fork sa created.
This file should not be merged with Dev

```
git clone --recurse-submodules https://gitlab.cern.ch/cms_tk_ph2/TTC_FC7_Control.git
cd TTC_FC7_Control
source setup.sh
mkdir build ; cd build
cmake ..
make -j X
```

If you have access this is also on xtaldaq@ph2acf02:/home/xtaldaq/TTC_FC7_developing/Ph2_ACF_forkXanda/

## To run tests on the TTC_FC7 module

```
mkdir $TTC_FC7_BASE_DIR/test_ttc_fc7 ; cd $TTC_FC7_BASE_DIR/test_ttc_fc7
```

- Upload the bistream

```
fpgaconfig_TTC_FC7 -i 2021.09.10_veszpv_v17.bit -c ../settings/TTC_FC7Description_skeleton.xml
fpgaconfig -i d19c_2s_cic2_lpgbt0_Sarah.bin -c ../../Ph2_ACF_forkXanda/settings/D19CDescription_2Sskeleton.xml
```

- Run the module

```
ttc_fc7_module_test -f ../settings/TTC_FC7Description_skeleton.xml --fullRun
```

See help `-h` for other options of actions.
The options for actions are cumulative)

- If you made a scan on trigger frequencies, the analysis tools to find the threshold are still on plain python. In the same folder you ran the results you do:

```
python3 ../scripts/timming_plots.py
```

You need to have scipy installed (do `python -m pip install --user scipy` or ` pip install scipy --user`).

## Launching an Eudaq producer

1) Clone and compile the eudaq framework, just run the following commands in the parent directory from Ph2_ACF :

```
git clone --single-branch --branch=cms_tk_master https://gitlab.cern.ch/cms_tk_ph2/eudaq.git eudaq
cd eudaq
mkdir build
cd build
cmake ..
make install
```

By advice of  we are using the latest developments of eudaq package in the `cms_tk_master` branch. The Cmake file is modifyed to account to that.

2) Recompile TTC_FC7_Control with eudaq as an external package

```
cd TTC_FC7_Control
source setup.sh
source eudaq_setup.sh
cd build
cmake ..
make -j X
```

3) To launch the producer GUI

```
mkdir test_eudaq
cd test_eudaq
./../scripts/eudaq_test.sh
```

This will launch the GUI. right after, in the same terminal, connect the producer:

```
euCliCollector -n EventIDSyncDataCollector -t one_dc -r tcp://localhost:44000 &
eudaqproducer_TTC_FC7 -r tcp://localhost:44000 &
```

p.s.: if you give the option `-n Something` you should change `Producer.ttcfc7producer` [here](https://gitlab.cern.ch/cms_tk_ph2/TTC_FC7_Control/-/blob/master/EUDAQcfg/telescope.ini#L2) to `Producer.Something`

Your ssh session should allow for graphical interfaces.
In the graphical interface you will be able to load the configuration files, then use the buttons to initialize, configure, .... The configuration files to be usead are in:

 - init file: `EUDAQcfg/beam.conf`
 - config file: `EUDAQcfg/telescope.ini`

Right now it saves the data on our custom format
